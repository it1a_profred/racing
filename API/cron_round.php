<?php
/**
 * Created by PhpStorm.
 * User: TP
 * Date: 8/24/2018
 * Time: 12:54 PM
 */


$servername = "localhost";
$username = "root";
//$username = "trieucong";
$password = "";
//$password = "trieucongcong";
$db = "game";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $db);

if (!$conn) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "Success!";
}

//Get ID Number ROUND
$select_query = "SELECT id FROM hl_rounds ORDER BY timestart desc LIMIT 1";
$result = $conn->query($select_query);
$data = $result->fetch_assoc();

//Update Status Round [Waiting --> Clearing]
$update_query_rounds = "UPDATE hl_rounds SET status = '1' WHERE id =" . $data['id'];
$conn->query($update_query_rounds);

//Update RandomResult
$numbers = range(1, 10);
shuffle($numbers);
$random_array = array_slice($numbers, 0, 10);
$string_result = implode(",", $random_array);
//Result Odd Event
odd_even_results($random_array, $data['id']);
//Tiger Dragon Event
$TD_Array_15 = array_slice($random_array, 0, 5, true);
$TD_Array_Bot = array_slice($random_array, 5, 5, true);
$TD_Array_510 = array_reverse($TD_Array_Bot);
//Result Tiger Dragon Bet
TD_results($TD_Array_15, $TD_Array_510, $data['id']);
//Result Big Small Bet
big_small_results($random_array, $data['id']);
//Result Rank Bet
Rank_bet_result($random_array, $data['id']);
//Champion Bet
champion_bet_result($random_array, $data['id']);
//Update Coin
calculate_coin($data['id']);
$update_result_rounds = "UPDATE hl_rounds SET result = '$string_result' WHERE id =" . $data['id'];
$conn->query($update_result_rounds);

//--Custom Here--
function connect()
{
    $servername = "localhost";
    $username = "root";
//$username = "trieucong";
    $password = "";
//$password = "trieucongcong";
    $db = "game";
// Create connection
    $conn = mysqli_connect($servername, $username, $password, $db);
    return $conn;
}

//Solve Win/Lose [Single/Double]
function odd_even_results($array, $round_id)
{
    $conn = connect();
    $query_bet = "SELECT * FROM hl_bet WHERE round_id = $round_id";
    $result = $conn->query($query_bet);
    if ($result->num_rows > 0) {
        $node = array();
        while ($row2 = $result->fetch_assoc()) {
            $node[] = $row2;
        }
        $result_string = array();
        foreach ($array as $k => $result) {
            if ($result % 2 == 0) {
                $result_string[] = 'DS' . ($k + 1) . '_S';
            } else {
                $result_string[] = 'DS' . ($k + 1) . '_D';
            }
        }

        $data_resol = array();
        foreach ($node as $value) {
            $data_resol[$value['id']] = json_decode($value['data_bet'], true);
        }
        foreach ($data_resol as $key => $value) {
            $check = array();
            foreach ($value as $bet_type => $coin_bet) {
                if (in_array($bet_type, $result_string)) {
                    $check[$key][] = $coin_bet * 1.9999;
                } else {
                    $check[$key][] = 0;
                }
            }
            $parse_data = array();
            foreach ($check as $id => $item) {
                $parse_data[$id] = implode(",", $item);
                $query_check_result = "SELECT result_o_v FROM hl_bet WHERE id = $id";
                $result_check = $conn->query($query_check_result);
                $data_check = $result_check->fetch_assoc();
                if ($data_check['result_o_v'] == 0) {
                    $query_update = "UPDATE hl_bet SET result_o_v = '$parse_data[$id]' WHERE id=$id";
                    $conn->query($query_update);
                }
            }
        }
        return true;
    } else {
        return false;
    }
}

//Solve Tiger/Dragon Bet
function TD_results($array15, $array510, $round_id)
{

    $conn = connect();
    $query_bet = "SELECT * FROM hl_bet WHERE round_id = $round_id";
    $result = $conn->query($query_bet);

    if ($result->num_rows > 0) {
        $node = array();
        while ($row2 = $result->fetch_assoc()) {
            $node[] = $row2;
        }

        $result_string = array();
        foreach ($array15 as $k => $result) {
            if ($result > $array510[$k]) {
                $result_string[] = 'LH' . ($k + 1) . '_L';
            } else {
                $result_string[] = 'LH' . ($k + 1) . '_H';
            }
        }
        $data_resol = array();
        foreach ($node as $value) {

            $data_resol[$value['id']] = json_decode($value['data_bet'], true);
        }
        foreach ($data_resol as $key => $value) {
            $check = array();
            foreach ($value as $bet_type => $coin_bet) {
                if (in_array($bet_type, $result_string)) {
                    $check[$key][] = $coin_bet * 1.9999;
                } else {
                    $check[$key][] = 0;
                }
            }
            $parse_data = array();
            foreach ($check as $id => $item) {
                $parse_data[$id] = implode(",", $item);
                $query_check_result = "SELECT result_t_d FROM hl_bet WHERE id = $id";
                $result_check = $conn->query($query_check_result);
                $data_check = $result_check->fetch_assoc();
                if ($data_check['result_t_d'] == 0) {
                    $query_update = "UPDATE hl_bet SET result_t_d = '$parse_data[$id]' WHERE id=$id";
                    $conn->query($query_update);
                }
            }
        }
        return true;
    } else {
        //$data_bet = $query_bet->data_bet;
        return false;
    }
}

//Solve Win/Lose [Big/Small]
function big_small_results($array, $round_id)
{
    $conn = connect();
    $query_bet = "SELECT * FROM hl_bet WHERE round_id = $round_id";
    $result = $conn->query($query_bet);

    if ($result->num_rows > 0) {
        $node = array();
        while ($row2 = $result->fetch_assoc()) {
            $node[] = $row2;
        }
        $result_string = array();
        foreach ($array as $k => $result) {
            if ($result > 5) {
                $result_string[] = 'DX' . ($k + 1) . '_D';
            } else {
                $result_string[] = 'DX' . ($k + 1) . '_X';
            }
        }

        $data_resol = array();
        foreach ($node as $value) {
            $data_resol[$value['id']] = json_decode($value['data_bet'], true);
        }
        foreach ($data_resol as $key => $value) {
            $check = array();
            foreach ($value as $bet_type => $coin_bet) {
                if (in_array($bet_type, $result_string)) {
                    $check[$key][] = $coin_bet * 1.9999;
                } else {
                    $check[$key][] = 0;
                }
            }
            $parse_data = array();
            foreach ($check as $id => $item) {
                $parse_data[$id] = implode(",", $item);
                $query_check_result = "SELECT result_b_s FROM hl_bet WHERE id = $id";
                $result_check = $conn->query($query_check_result);
                $data_check = $result_check->fetch_assoc();
                if ($data_check['result_b_s'] == 0) {
                    $query_update = "UPDATE hl_bet SET result_b_s = '$parse_data[$id]' WHERE id=$id";
                    $conn->query($query_update);
                }
            }
        }
        return true;
    } else {
        //$data_bet = $query_bet->data_bet;
        return false;
    }
}

function Rank_bet_result($array, $round_id)
{
    $conn = connect();
    $query_bet = "SELECT * FROM hl_bet WHERE round_id = $round_id";
    $result = $conn->query($query_bet);

    if ($result->num_rows > 0) {
        $node = array();
        while ($row2 = $result->fetch_assoc()) {
            $node[] = $row2;
        }
        $result_string = array();
        foreach ($array as $k => $result) {
            $result_string[] = 'B' . ($k + 1) . '_' . $result;

        }
        $data_resol = array();
        foreach ($node as $value) {
            $data_resol[$value['id']] = json_decode($value['data_bet'], true);
        }

        foreach ($data_resol as $key => $value) {
            $check = array();
            foreach ($value as $bet_type => $coin_bet) {
                if (in_array($bet_type, $result_string)) {
                    $check[$key][] = $coin_bet * 9.93;
                } else {
                    $check[$key][] = 0;
                }
            }
            $parse_data = array();
            foreach ($check as $id => $item) {
                $parse_data[$id] = implode(",", $item);
                $query_check_result = "SELECT result_rank_bet FROM hl_bet WHERE id = $id";
                $result_check = $conn->query($query_check_result);
                $data_check = $result_check->fetch_assoc();
                if ($data_check['result_rank_bet'] == 0) {
                    $query_update = "UPDATE hl_bet SET result_rank_bet = '$parse_data[$id]' WHERE id=$id";
                    $conn->query($query_update);
                }
            }
        }
        return true;
    } else {
        //$data_bet = $query_bet->data_bet;
        return false;
    }
}

//Solve Bet Champion
function champion_bet_result($array, $round_id)
{
    $conn = connect();
    $query_bet = "SELECT * FROM hl_bet WHERE round_id = $round_id";
    $result = $conn->query($query_bet);

    if ($result->num_rows > 0) {
        $node = array();
        while ($row2 = $result->fetch_assoc()) {
            $node[] = $row2;
        }
        $sum_2_car = $array[0] + $array[1];
        print_r($sum_2_car);
        $result_string = array();
        if ($sum_2_car > 3 && $sum_2_car <= 11) {
            $result_string[0] = 'GDX_X';
        } else {
            $result_string[0] = 'GDX_D';
        }
        if ($sum_2_car % 2 == 0) {
            $result_string[1] = 'GDS_S';
        } else {
            $result_string[1] = 'GDS_D';
        }
        $data_resol = array();
        foreach ($node as $value) {
            $data_resol[$value['id']] = json_decode($value['data_bet'], true);
        }
        foreach ($data_resol as $key => $value) {
            $check = array();
            foreach ($value as $bet_type => $coin_bet) {
                if (in_array($bet_type, $result_string)) {
                    if ($bet_type == 'GDX_D' || $bet_type == 'GDS_S') {
                        $check[$key][] = $coin_bet * 2.2;
                    } elseif ($bet_type == 'GDX_X' || $bet_type == 'GDS_D') {
                        $check[$key][] = $coin_bet * 1.79;
                    }

                } else {
                    $check[$key][] = 0;
                }
            }
            $parse_data = array();
            foreach ($check as $id => $item) {
                $parse_data[$id] = implode(",", $item);
                $query_check_result = "SELECT result_champion_bet FROM hl_bet WHERE id = $id";
                $result_check = $conn->query($query_check_result);
                $data_check = $result_check->fetch_assoc();
                if ($data_check['result_champion_bet'] == 0) {
                    $query_update = "UPDATE hl_bet SET result_champion_bet = '$parse_data[$id]' WHERE id=$id";
                    $conn->query($query_update);
                }
            }
        }
    }

}

//Calculate Coin
function calculate_coin($round_id)
{
    $conn = connect();
    $query_select = "SELECT result_champion_bet,result_rank_bet,result_o_v,result_t_d,result_b_s,user_id FROM hl_bet WHERE round_id = '$round_id' AND status= 1";
    $result = $conn->query($query_select);
    if ($result->num_rows > 0) {
        $node = array();
        while ($row2 = $result->fetch_assoc()) {
            $node[] = $row2;
        }
        foreach ($node as $key => $value) {
            //$query_user_id = $this->db->select('id,coin')->from('hl_users')->where('id', $value->user_id)->get()->row();
            $query_user_id = "SELECT id, coin FROM hl_users WHERE id=" . $value['user_id'];
            $result_user_id = $conn->query($query_user_id);

            $data_info = $result_user_id->fetch_assoc();

            $json_result_o_v = $value['result_o_v'];
            $json_result_t_d = $value['result_t_d'];
            $json_result_b_s = $value['result_b_s'];
            $json_result_champion_bet = $value['result_champion_bet'];
            $json_result_rank_bet = $value['result_rank_bet'];

            $coin_bet_result_o_v = 0;
            $coin_bet_result_t_d = 0;
            $coin_bet_result_b_s = 0;
            $coin_bet_result_rank_bet = 0;
            $coin_bet_result_champion_bet = 0;

            $data_result_o_v = explode(",", $json_result_o_v);
            $data_result_t_d = explode(",", $json_result_t_d);
            $data_result_b_s = explode(",", $json_result_b_s);
            $data_result_rank_bet = explode(",", $json_result_rank_bet);
            $data_result_champion_bet = explode(",", $json_result_champion_bet);

            foreach ($data_result_o_v as $result) {
                $coin_bet_result_o_v += $result;
            }

            foreach ($data_result_b_s as $result) {
                $coin_bet_result_b_s += $result;
            }

            foreach ($data_result_t_d as $result) {
                $coin_bet_result_t_d += $result;
            }
            foreach ($data_result_rank_bet as $result) {
                $coin_bet_result_rank_bet += $result;
            }
            foreach ($data_result_champion_bet as $result) {
                $coin_bet_result_champion_bet += $result;
            }
            $after_coin = $data_info['coin'] + $coin_bet_result_o_v + $coin_bet_result_t_d + $coin_bet_result_b_s + $coin_bet_result_rank_bet+ $coin_bet_result_champion_bet;
            $query_update = "UPDATE hl_users SET coin = $after_coin WHERE id =" . $data_info['id'];
            $conn->query($query_update);
        }
        return $query_update;
    } else {
        return false;
    }
}

//--End Cusom--


//Update Status Bet in hl_bet
$update_query = "UPDATE hl_bet SET status = '1' WHERE round_id =" . $data['id'];
$conn->query($update_query);
//print_r($data['id']);
//exit;


//Create new Round when time over
$str_time_start = time() + 3 * 60;
$time_start = date('Y-m-d H:i', $str_time_start);
$round_name = "BHLR_" . sprintf("%04d", $data['id'] + 1);
$sql = "INSERT INTO hl_rounds (round_name, timestart,status) VALUES ('$round_name',$str_time_start,0)";
$result = $conn->query($sql);
if ($result == true) {
    echo "Successfully!";
} else {
    echo "Insert failed";
}
$txt = "Time start: " . $time_start . "\n" . calculate_coin($data['id']) . "\n";
$myfile = file_put_contents('log.txt', $txt . PHP_EOL, FILE_APPEND | LOCK_EX);