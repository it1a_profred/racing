<div id="header" class="header">
    <div class="logo"><img src="logo/best-bet-logo.png" alt="Logo-bet" height="46px" width="auto"/></div>
    <div class="top">
        <div class="menu">
            <div class="menu1">
                <div id="result_info" class="draw_number">
                    <div>HL Racing(HLR10)</div>
                    <div>Round ID: <?php echo $ticket_id - 1; ?></div>
                </div>
                <a id="result_balls" target="_blank" href="../demo/recently-result.php"
                   class="T_PK10 L_BJPK10" style="margin-top: -5px;">
                    <?php
                    $fake_query = "SELECT result FROM hl_rounds WHERE id=" . ($ticket_id - 1);
                    $fake_data = $conn->query($fake_query);
                    $fake_result = $fake_data->fetch_assoc();
                    if ($fake_result['result'] != null) {
                        $fake_array = explode(",", $fake_result['result']);
                        foreach ($fake_array as $value) {
                            ?>
                            <span><b class="b<?php echo $value; ?>"><?php echo $value; ?></b></span>
                        <?php }
                        $fake_sum = $fake_array[1] + $fake_array[0];
                        ?>
                        <div class="result_stat clearfix">
                            <div class="statitm"><?php echo $fake_sum; ?></div>
                            <div class="statitm"><?php echo ($fake_sum > 11) ? "Big" : "Small"; ?></div>
                            <div class="statitm"><?php echo ($fake_sum % 2 == 0) ? "Even" : "Odd"; ?></div>
                            <div
                                class="statitm"><?php echo ($fake_array[0] > $fake_array[9]) ? "Dragon" : "Tiger"; ?></div>
                            <div
                                class="statitm"><?php echo ($fake_array[1] > $fake_array[8]) ? "Dragon" : "Tiger"; ?></div>
                            <div
                                class="statitm"><?php echo ($fake_array[2] > $fake_array[7]) ? "Dragon" : "Tiger"; ?></div>
                            <div
                                class="statitm"><?php echo ($fake_array[3] > $fake_array[6]) ? "Dragon" : "Tiger"; ?></div>
                            <div
                                class="statitm"><?php echo ($fake_array[4] > $fake_array[5]) ? "Dragon" : "Tiger"; ?></div>
                        </div>
                    <?php } ?>
                </a>
            </div>
            <?php if (!empty($_SESSION['user_id'])) { ?>
                <div class="menu2">
                    <!--                <span><a target="frame" href="bets">未结明细 </a></span> |-->
                <span><a target="frame"
                         href="member/load/bet-history.php<?php echo (!empty($_SESSION['user_id'])) ? "?user_id=" . $_SESSION['user_id'] : "" ?>">[History]</a></span>
                    |
                    <!--                <span><a href="bets?settled=true" target="frame">今天已结</a></span> |-->
                <span><a
                        href="member/load/bet-running.php<?php echo (!empty($_SESSION['user_id'])) ? "?user_id=" . $_SESSION['user_id'] : "" ?>"
                        target="frame">[Running]</a></span> |
                    <span><a
                            href="member/load/deposit.php<?php echo (!empty($_SESSION['user_id'])) ? "?user_id=" . $_SESSION['user_id'] : "" ?>"
                            target="frame">[Deposit]</a></span> |
                    <span><a
                            href="member/load/withdraw.php<?php echo (!empty($_SESSION['user_id'])) ? "?user_id=" . $_SESSION['user_id'] : "" ?>"
                            target="frame">[Withrawal]</a></span> <br>
                    <!-- <span><a target="frame" href="#"></a></span> -->
                </div>
            <?php } ?>
            <div class="menu4"><a target="_blank"
                                  href="https://chatserver.comm100.com/chatWindow.aspx?siteId=228557&amp;planId=61"
                                  class="support"></a></div>
            <div class="menu3">
                <?php if (empty($_SESSION)) { ?>
                    <div class="login">
                        <form method="post" action="">
                            <div class="userid form-control">
                                <input type="text" id="username" name="user_name" placeholder="User Name" tabindex="1">
                            </div>
                            <div class="password form-control">
                                <input type="password" id="password" name="password" placeholder="Password"
                                       tabindex="2">
                            </div>
                            <button type="submit" id="btnlogin" name='login' class="btn btn-red" tabindex="4">
                                <span>Login</span>
                            </button>
                        </form>

                        <!--           <div class="themepicker">
                                     <div class="themeicon icon1 icon1active" rel="blue"></div>
                                     <div class="themeicon icon2" rel="gold"></div>
                                     <div class="themeicon icon3" rel="red"></div>
                                  </div> -->
                    </div>
                    <div class="forgot-password"><a href="#">Forgot Password?</a></div>
                    <div class="forgot-password"><a href="#register-form" rel="modal:open">Register</a></div>
                <?php } else { ?>
                    <form action="" method="post">
                        <button type="submit" name="logout" class="logout">
                            <div>Log out</div>
                        </button>
                    </form>
                <?php } ?>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div class="lotterys">
            <div class="menucontainer">
                <div class="spritearrow arrowup"></div>
                <p class="btnMenu"></p>
                <div id="lotterys" style="display: none">
                    <a href="javascript:void(0)" id="l_BJPK10" lang="PK10_0" rel="0">

                        <?php if (empty($_SESSION['user_id'])) { ?>

                            Homepage

                        <?php } else {
                            echo "<span class='lotterysHot'>";
                            echo "HL Racing(HR10)";
                            echo "</span>";
                        } ?>

                    </a>
                </div>
                <div class="show navMenu">
                    <ul class="items" id="items">
                        <li><a href="#"><span>Demo bet</span></a></li>
                        <!-- <li><a href="#"><span>Chat Room</span></a></li> -->
                        <li><a href="#"><span>Sign up Now!</span></a></li>
                        <li><a href="#"><span>Download APP</span></a></li>
                        <li><a href="#"><span>Jackpot</span></a></li>
                        <li><a href="#"><span>Result Page</span></a></li>
                        <!-- <li><a href="#"><span>Latest announces</span></a></li> -->

                    </ul>
                </div>
                <!-- <div class="menumoregame">
                   <div id="moregames">更多游戏</div>
                   <div id="moregameicon">▼</div>
                </div> -->
            </div>
            <div class="gamecontainer">
                <div style="height: 20px;"></div>
                <div class="gamebox clearfix" style="display:block">
                </div>
                <div class="gamesmltxt">
                    注：已选择的彩种可通过鼠标拖动改变排列顺序。
                </div>
                <div class="editon">
                    <button class="gamebtn1">编辑</button>
                </div>
                <div class="editoff" style="display: none">
                    <button class="gamebtn2">取消</button>
                    <button class="gamebtn1">确定</button>
                </div>
            </div>
        </div>

        <div class="sub">
            <div id="sub_BJPK10" style="display:none">
                <?php if (!empty($_SESSION['user_id'])) { ?>
                    <a href="member/load/index.php?lottery=BJPK10&page=lm<?php echo (!empty($_SESSION['user_id'])) ? "&user_id=" . base64_encode($_SESSION['user_id']) : "" ?>">Two
                        side
                        Bet [Type 1]</a> |
                    <a href="member/load/index2.php?lottery=BJPK10&page=110<?php echo (!empty($_SESSION['user_id'])) ? "&user_id=" . base64_encode($_SESSION['user_id']) : "" ?>">Rank
                        Bet [Type 2]</a>
                <?php } else {
                    ?>
                    <a href="member/index.php">
                    </a>
                    <?php
                } ?>
            </div>
        </div>


    </div>
</div>
<div id="register-form"  class="modal">
    <form class="formRegister blockForm" method="post" action="">
        <p class="textInput"><label for="reg_username">Username: </label><input type="text" minlength=4 name="reg_username" id="reg_username" required placeholder="Username"></p>
        <p class="textInput"><label for="reg_password">Password: </label><input type="password" name="reg_password" id="reg_password" minlength=6 required placeholder="Password"></p>
        <p class="textInput"><label for="reg_confirm_password">Confirm Password: </label><input type="password" name="reg_confirm_password" minlength=6 id="reg_confirm_password" required placeholder="Confirm Password"></p>
        <p class="btn"><button name="register" value="1">Submit</button></p>
    </form>
</div>
<script>
    $(document).ready(function() {
        var password = document.getElementById("reg_password")
          , confirm_password = document.getElementById("reg_confirm_password");

        function validatePassword(){
          if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
          } else {
            confirm_password.setCustomValidity('');
          }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    });
</script>