<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href=https://img.sg88.ws/dsn/dsn_favicon.ico/>
    <title>HL Racing Site</title>
    <link rel="stylesheet" type="text/css" href="newdsn/css/main.css?v=0106"/>
    <link rel="stylesheet" type="text/css" href="newdsn/css/balls.css"/>
    <link rel="stylesheet" type="text/css" href="newdsn/css/notice_popup.css?v=1221"/>
    <link rel="stylesheet" type="text/css" href="newdsn/css/vip.css"/>
    <link rel="stylesheet" type="text/css" href="newdsn/css/custom.css"/>
    <link rel="stylesheet" type="text/css" href="js/colorbox/colorbox.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery.modal.css"/>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>

    <!--    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
    <!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
    <!--    <script type="text/javascript" src="js/custom.js"></script>-->
    <script type="text/javascript" src="js/libs.js"></script>
    <script type="text/javascript" src="js/colorbox/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="newdsn/js/custompage.js"></script>
    <script type="text/javascript" src="js/json2.js"></script>
    <script type="text/javascript" src="js/swfobject.js"></script>
    <script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
    <!--      <script type="text/javascript" src="newdsn/js/notice_popup.js?v=1219"></script>-->
    <script type="text/javascript" src="newdsn/js/member.js?v=1219"></script>
    <script type="text/javascript" src="default/js/skin.js?v=1219"></script>
    <script type="text/javascript" src="default/js/notices.js"></script>
    <script type="text/javascript" src="newdsn/js/hk6Base.js"></script>
    <script type="text/javascript" src="newdsn/js/drawurls.js"></script>
    <script type="text/javascript" src="newdsn/js/cash/setSecurityQues.js"></script>
    <script type="text/javascript" src="newdsn/js/cash/vipcallback.js"></script>
    <script type="text/javascript" src="js/dialog.js"></script>
    <script type="text/javascript" src="js/jquery.modal.js"></script>
    <script>
        $(document).ready(function() {
            $('.btnMenu').click(function(event) {
                $('.navMenu').stop().fadeToggle();
            });
        });
    </script>
</head>
<?php
include('connect.php');
$conn = connect_database();
session_start();
if (isset($_POST['login'])) {
    $username = $_POST['user_name'];
    $password = md5($_POST['password']);
    $query_login = "SELECT * FROM hl_users WHERE user_name = '$username' AND password = '$password'";
    $result = $conn->query($query_login);
    $data_session = $result->fetch_assoc();
//    exit;
    $page_index = $_SERVER['PHP_SELF'];
    if ($result->num_rows > 0) {
        $_SESSION['user_id'] = $data_session['id'];
        $_SESSION['user_name'] = $data_session['user_name'];
        $_SESSION['email'] = $data_session['email'];
        header("Location: index.php");
        $log_error = 0;
    } else {
        $log_error = 1;
    }

} elseif (isset($_POST['logout'])) {
    header("Location: index.php");
    session_destroy();
}
if (isset($_POST['register'])) {

    $username = trim($_POST['reg_username']);
    $password = md5($_POST['reg_password']);
    $cfpassword = md5($_POST['reg_confirm_password']);
    $query_login = "SELECT * FROM hl_users WHERE user_name = '$username' AND password = '$password'";
    $result = $conn->query($query_login);
    $data_session = $result->fetch_assoc();
    $msgError = '';
    if ($password != $cfpassword) {
        $msgError = "Password does not match with password for confirmation.";
    }
    if ($result->num_rows > 0) {
        $msgError = 'This email address is already used. Please choose another.';
    }
    if ($msgError == '') {
        $sql = "INSERT INTO hl_users (user_name, password) VALUES ('" . $username . "','" . $password . "')";
        $result = $conn->query($sql);
        $page_index = $_SERVER['PHP_SELF'];
        if ($result == true) {
            $_SESSION['user_id'] = $conn->insert_id;
            $_SESSION['user_name'] = $username;
            $_SESSION['email'] = '';
            header("Location: index.php");
            $log_error = 0;
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }


}
$select_query = "Select * FROM hl_rounds ORDER BY timestart desc LIMIT 1";
$result = $conn->query($select_query);
$data_round = $result->fetch_assoc();
$time_bet = $data_round['timestart'];
$ticket_id = $data_round['id'];

function randomGen($min, $max, $quantity)
{
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}

if (@$msgError) {
    echo "<script>
        alert('" . $msgError . "');
    </script>";
}
?>
<script>
    $(document).ready(function () {
        var bet_value = $('#bet_value').val();
        $('#running-bet').text(bet_value);
    });
</script>
<body class="skin_blue">
<?php include 'include/header.php' ?>
<div id="main">
    
    <?php if(!empty($_SESSION['user_id'])) {?>
    <div class="blockFrame">
        <div class="side_left" id="side">
            <?php if (!empty($_SESSION)) { ?>
                <div class="top_bet leftpanel">
                    <div class="lpheader">
                        <div class="lpheadertitle">Account Information</div>
                        <a class="notification" style="display: none" href="member/center/notices?displayLatestFlag=true"
                           target="frame">
                            <span class="count"></span>
                        </a>
                        <input type="hidden" id="hiddenCount" name="hiddenCount" value="0"/>
                    </div>
                    <div class="lpcontent" id="account">
                        <b>Name</b>: <?php echo $_SESSION['user_name']; ?>
                        <br>
                        <b>Status</b>: <?php
                        $query_select = "SELECT * FROM hl_users WHERE id =" . $_SESSION['user_id'];
                        $result = $conn->query($query_select);
                        $data = $result->fetch_assoc();
                        if ($data['status'] == 1) {
                            echo "<b style='color:green;'>Actived</b>";
                        } else {
                            echo "<b style='color:red;'>Inactive</b>";
                        }
                        ?>
                        <b>Balance</b>：<span class="balance">
                            <?php
                            print_r($data['coin']);
                            ?></span><br>
                        <b>Running Bet：</b><span class="betting" id="running-bet">0</span>
                        <input type="hidden" id="userStatus" value="0"/>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($_SESSION['user_id'])) { ?>
                <div class="lpheader2">Recently Bet</div>

                <div class="betdone" id="lastBets">
                    <ul class="bets">
                        <?php
                        $sql = "SELECT * FROM hl_bet WHERE status = 0 and user_id ='" . $_SESSION['user_id'] . "' ORDER BY created_at DESC";
                        $result = $conn->query($sql);
                        $node_bet = array();
                        if (isset($_SESSION['user_id'])) {
                            while ($row2 = $result->fetch_assoc()) {
                                $node_bet[] = $row2;
                            }
                        }
                        if ($result->num_rows > 0) {
                            $value_bet = 0;
                            foreach ($node_bet as $key_bet => $info) {
                                $data = $result->fetch_assoc();
                                $json_data = json_decode($info['data'], true);
                                $val_bet = array_sum($json_data);
                                $value_bet += $val_bet;
                                $record = array();
                                foreach ($json_data as $type_id => $value) {

                                    if ($value) {
                                        ?>
                                        <?php if (!empty($_SESSION)) { ?>
                                            <li><p>Round ID: <span class="bid"><?php echo $ticket_id; ?></span></p>
                                                <?php
                                                $bet_type = $type_id;
                                                $bet_value = $value;
                                                //BET TYPE 2
                                                $type_2 = substr($bet_type, 0, 1);
                                                $place_type_2 = substr($bet_type, strpos($bet_type, 'B') + 1, strpos($bet_type, '_') - 1);
                                                $place_horse_2 = substr($bet_type, strpos($bet_type, '_') + 1);
                                                $place_2_text = '';
                                                if ($type_2 == 'B') {
                                                    if ($place_type_2 == 1) {
                                                        $place_2_text = '1' . "st";
                                                    } elseif ($place_type_2 == 2) {
                                                        $place_2_text = '2' . "nd";
                                                    } elseif ($place_type_2 == 3) {
                                                        $place_2_text = '3' . "rd";
                                                    } else {
                                                        $place_2_text = $place_type_2 . "th";
                                                    }
                                                    echo "<p class='contents'>" .
                                                        "<span class='text'>" .
                                                        $place_2_text . " Place" . "[ Car: " . '<b>' . $place_horse_2 . '</b>' . "]" . "</span><br/>" . "Rate: <span class='odds'>9.93</span></p>";
                                                    echo "<p>Bet Amount: " . "<b style='color:#9ba043'>" . $bet_value . '</b>' . "</p>";
                                                } else {
                                                    //BET TYPE 1
                                                    $rate = 1.9999;
                                                    $type = substr($bet_type, 0, 2);
                                                    $place = substr($bet_type, 2, 1);
                                                    $place_text = '';
                                                    if ($place > 3) {
                                                        $place_text = $place . "th";
                                                    } elseif ($place == 2) {
                                                        $place_text = $place . "nd";
                                                    } elseif ($place == 3) {
                                                        $place_text = $place . "rd";
                                                    } elseif ($place == 1) {
                                                        $place_text = $place . "st";
                                                    }
                                                    $pick = substr($bet_type, 4);
                                                    $text = '';
                                                    if ($type == "DS") {
                                                        if ($pick == "D") {
                                                            $text = "Odd";
                                                        } else {
                                                            $text = "Even";

                                                        }
                                                    } elseif ($type == "DX") {
                                                        if ($pick == 'D') {
                                                            $text = "Big";
                                                        } else {
                                                            $text = "Small";
                                                        }
                                                    } elseif ($type == "LH") {
                                                        if ($pick == 'L') {
                                                            $text = "Dragon";
                                                        } else {
                                                            $text = "Tiger";
                                                        }
                                                    } elseif ($type == 'GD') {
                                                        $text = 'Champion Bet';
                                                        $type_champion = substr($bet_type, 0, 3);
                                                        if ($type_champion == 'GDX') {
                                                            if (substr($bet_type, 4, 1) == 'D') {
                                                                $rate = 2.2;
                                                                $place_text = 'Big';
                                                            } else {
                                                                $rate = 1.79;
                                                                $place_text = 'Small';
                                                            }
                                                        } else {
                                                            if (substr($bet_type, 4, 1) == 'D') {
                                                                $rate = 1.79;
                                                                $place_text = 'Odds';
                                                            } else {
                                                                $rate = 2.2;
                                                                $place_text = 'Even';
                                                            }
                                                        }
                                                    }
                                                    echo "<p class='contents'>" .
                                                        "<span class='text'>" .
                                                        $place_text . " Place" . "[" . $text . "]" . "</span><br/>" . "Rate: <span class='odds'>$rate</span></p>";
                                                    echo "<p>Bet Amount: " . "<b style='color:#9ba043'>" . $bet_value . '</b>' . "</p>";

                                                }
                                                ?>
                                            </li>
                                        <?php } ?>
                                        <?php
                                    }
                                }
                            }
                            echo "<input id='bet_value' value='$value_bet' type='hidden'/>";
                        } else {
                            if (!empty($_SESSION['user_id'])) {
                                echo "<li style='text-align: center;'><i>No Data BET </i></li>";
                            }
                        }
                        ?>
                    </ul>
                </div>
            <?php } ?>
            <div style="display:none" id="betResultPanel">
                <div class="bresults" id="print-bet">
                    <ul class="bets" id="betReulstList"></ul>
                    <table class="total s0">
                        <tbody>
                        <tr>
                            <td class="label">下注期数</td>
                            <td id="betResultDrawNumber"></td>
                        </tr>
                        <tr>
                            <td class="label">下注金额</td>
                            <td id="betResultCount"></td>
                        </tr>
                        <tr>
                            <td class="label">合计金额</td>
                            <td id="betResultTotal"></td>
                        </tr>
                        <tr>
                            <td><input type="button" class="button" value="打 印" onclick="printBet('print-bet')"></td>
                            <td><input type="button" class="button" value="返 回" onclick="resetPanel()"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="frame"
             style="-webkit-overflow-scrolling:touch;overflow:auto;<?php echo (empty($_SESSION['user_id'])) ? "left:0px!important;" : "" ?>">
            <iframe id="frame" name="frame" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <?php }else{
        include 'member/index.php';
    } ?>
</div>
<?php include 'include/footer.php' ?>
<div id="settingbet" title="快选金额" style="display:none;">
    <input name="bet_1" placeholder="快选金额" class="ds"/><br/>
    <input name="bet_2" placeholder="快选金额" class="ds"/><br/>
    <input name="bet_3" placeholder="快选金额" class="ds"/><br/>
    <input name="bet_4" placeholder="快选金额" class="ds"/><br/>
    <input name="bet_5" placeholder="快选金额" class="ds"/><br/>
    <input name="bet_6" placeholder="快选金额" class="ds"/><br/>
    <input name="bet_7" placeholder="快选金额" class="ds"/><br/>
    <input name="bet_8" placeholder="快选金额" class="ds"/><br/>
    <label>
        <input name="settingbet" type="radio" id="settingbet_0" value="1" checked="checked"/>
        启动</label>
    <label>
        <input name="settingbet" type="radio" id="settingbet_1" value="0"/>
        停用</label>
    <br/>
    <br/>
    <input type="button" class="button" value="储存" onClick="submitsetting()"/>
</div>
<div class="Notice"></div>
<div class="details">
    <div class="back_body"></div>
    <div id="dtlColor" class="details_div">
        <a href="#">
            <div class="close_icon"></div>
        </a>

        <div class="details_icon">
            <div></div>
        </div>
        <div id="dtlFont" class="details_font"></div>
    </div>
</div>
<div id="redPack" hidden>
    <div class="back_body"></div>
    <div class="redpack_body">
        <img src="newdsn/images/close.png" onClick="$('#redPack').hide();" class="redpack_close"/>

        <div class="redpack_amt_con">
            <span class="redpack_amount">0</span>
            <span class="redpack_currency"> 元</span>
        </div>
        <div class="redpack_remark"></div>
        <img src="newdsn/images/hongbao_popup.png"/>
    </div>
</div>
<div id="resultList" class="result_list" hidden>
    <div class="result_list_header">
        <span>弹窗可拖动</span>

        <div class="result_list_close" onClick="$('#resultList').hide();">&times;</div>
        <div id="resultListHeader" style="background:Gainsboro;text-align:center;font-weight: bold;"></div>
    </div>
    <div class="result_list_block"></div>
    <iframe id="resultFrame" name="resultFrame" class="result_frame" frameborder="0" allowfullscreen></iframe>
</div>
<div class="popup dialing">
    <div class="container">
            <span class="close-btn">
            <a href="#">✖</a>
            </span>
        <img class="popup-icon" src="newdsn/images/cash/icon.png"/>

        <div class="warning-wrapper">
            <p class="dialing-text">您的通话即将接通<span>.<span></p>

            <p class="dialing-info">请耐心等待客服接应，谢谢。</p>
        </div>
        <div class="tel-wrapper">
            <p class="tel-desc">用户手机号码:</p>

            <p class="tel"></p>
        </div>
        <button class="confirm-btn">知道</button>
    </div>
</div>
<div class="overlay"></div>
<link rel="stylesheet" type="text/css" href="newdsn/css/chatroom.css"/>
<script type="text/javascript" src="newdsn/js/chatroom.js"></script>
<div class="iframe-wrapper">
    <button class="close-iframe" onClick="closeIframe()">
        <svg class="close-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
             version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 212.982 212.982"
             style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="512px" height="512px">
               <g id="Close">
                   <path style="fill-rule:evenodd;clip-rule:evenodd;"
                         d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312   c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312   l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937   c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"
                         fill="#FFFFFF"/>
               </g>
            </svg>
    </button>
    <button class="new-tab-iframe" onclick="openNewTab()">
        <svg class="new-tab-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
             version="1.1" viewBox="0 0 24.1 24.1" enable-background="new 0 0 24.1 24.1" width="512px" height="512px">
            <g>
                <path
                    d="m23,14.05h-2c-0.6,0-1,0.4-1,1v4.5c0,0.3-0.2,0.5-0.5,0.5h-15c-0.3,0-0.5-0.2-0.5-0.5v-15c0-0.3 0.2-0.5 0.5-0.5h4.5c0.6,0 1-0.4 1-1v-2c0-0.6-0.4-1-1-1h-8c-0.6,0-1,0.4-1,1v22c0,0.6 0.4,1 1,1h22c0.6,0 1-0.4 1-1v-8c0-0.6-0.4-1-1-1z"
                    fill="#FFFFFF"></path>
                <path
                    d="m23,.05h-8c-0.6,0-1,0.4-1,1v2c0,0.6 0.4,1 1,1h1c0.4,0 0.7,0.5 0.4,0.9l-8,8c-0.4,0.4-0.4,1 0,1.4l1.4,1.4c0.4,0.4 1,0.4 1.4,0l8-8c0.3-0.3 0.9-0.1 0.9,0.4v1c0,0.6 0.4,1 1,1h2c0.6,0 1-0.4 1-1v-8c-0.1-0.7-0.5-1.1-1.1-1.1z"
                    fill="#FFFFFF"></path>
            </g>
        </svg>
    </button>
</div>
<script>
    if (typeof skinColor !== 'undefined') {
        setSkin(skinColor, $('body'));
    }
</script>
</body>
</html>