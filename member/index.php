<style>
    * {
        box-sizing: border-box;
    }

    body {
        font-family: Verdana, sans-serif;
    }

    .mySlides {
        display: none;
    }

    img {
        vertical-align: middle;
    }

    .footer {
        display: flex;
        height: 340px;
        margin-top: -36px;
        background: linear-gradient( rgba(182, 184, 193, 0.55), rgba(29, 28, 29, 0.75) ), #292727;
    }

    /* Slideshow container */
    .slideshow-container {
        /*max-width: 1000px;*/
        position: relative;
        margin: auto;
    }

    /* Caption text */
    .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
    }

    /* Number text (1/3 etc) */
    .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    /* The dots/bullets/indicators */
    .dot {
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
    }

    .active {
        background-color: #717171;
    }

    /* Fading animation */
    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 1.5s;
        animation-name: fade;
        animation-duration: 1.5s;
    }

    @-webkit-keyframes fade {
        from {
            opacity: .4
        }
        to {
            opacity: 1
        }
    }

    @keyframes fade {
        from {
            opacity: .4
        }
        to {
            opacity: 1
        }
    }

    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 300px) {
        .text {
            font-size: 11px
        }
    }
    @media only screen and (max-width: 736px) {
        .footer {
            height: auto;
            display: block;
        }
    }
</style>
<div style="background: url(newdsn/images/custom/DarkG-bg.jpg);">
<div class="slideshow-container">

    <div class="mySlides fade">
        <div class="numbertext">1 / 3</div>
        <img src="newdsn/images/home-banner-2.jpg" style="width:100%">
    </div>

    <div class="mySlides fade">
        <div class="numbertext">2 / 3</div>
        <img src="newdsn/images/home-banner-3.jpg" style="width:100%">
    </div>

    <div class="mySlides fade">
        <div class="numbertext">3 / 3</div>
        <img src="newdsn/images/home-banner-4.jpg" style="width:100%">
    </div>

</div>
<br>

<div style="text-align:center">
    <span class="dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
</div>

<script>
    var slideIndex = 0;
    showSlides();

    function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        setTimeout(showSlides, 2000); // Change image every 2 seconds
    }
</script>
<div class="footer">
    <div class="col1">
        <div class="col_subhead">
            <div class="col_icon1">
                <img src="newdsn/images/cash/icon_prize1.png" width="60%" alt="">
            </div>
            <div class="col_headtxt1"><img src="newdsn/images/custom/top-prize.png" width="121" height="26" alt="">
            </div>
        </div>
        <div class="col_img">
            <div class="bx-wrapper" style="max-width: 100%;">
                <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 154px;">
                    <ul class="bxslider2" style="padding-top: 10px; width: auto; position: relative;">
                        <li style="float: none; list-style: none; position: absolute; width: 460px; z-index: 50; display: block;">
                            <div class="prizecontainer">
                                <div class="col1">
                                    <a href="#">
                                        <img src="newdsn/images/custom/iphone-x.png" width="126" height="auto" alt=""
                                             class="clearfix">
                                    </a>
                                    <a href="#">
                                        <img src="newdsn/images/custom/S9.jpg" width="126" height="auto" alt=""
                                             class="clearfix">
                                    </a>
                                    <a href="#">
                                        <img src="newdsn/images/custom/nokia-1280.jpg" width="126" height="auto"
                                             alt=""
                                             class="clearfix">
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col2">
        <div class="col_subhead2">
            <div class="col_icon2">
                <img src="newdsn/images/cash/icon_prize2.png" width="60%" alt="">
            </div>
            <div class="col_headtxt2"><img src="newdsn/images/custom/prize.png" width="104" height="26" alt="">
            </div>
        </div>
        <div class="col_img2">
            <div class="bx-wrapper" style="max-width: 100%;">
                <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 154px;">
                    <ul class="bxslider2" style="padding-top: 10px; width: auto; position: relative;">
                        <li style="float: none; list-style: none; position: absolute; width: 460px; z-index: 50; display: block;">
                            <div class="prizecontainer">
                                <div class="col1">
                                    <a href="#">
                                        <img src="newdsn/images/custom/durex.jpg" width="126" height="126" alt=""
                                             class="clearfix">
                                    </a>
                                    <a href="#">
                                        <img src="newdsn/images/custom/helmet.jpg" width="126" height="126" alt=""
                                             class="clearfix">
                                    </a>
                                    <a href="#">
                                        <img src="newdsn/images/custom/water-gun.jpg" width="126" height="126"
                                             alt=""
                                             class="clearfix">
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
