<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" type="text/css" href="../../newdsn/css/table.css">
    <script type="text/javascript" src="../../js/jquery.js"></script>
    <script type="text/javascript" src="../../js/libs.js"></script>
    <script type="text/javascript" src="../../default/js/skin.js"></script>
    <script type="text/javascript">$(function () {
            LIBS.colorMoney('.color', 'minus')
        })</script>
</head>

<?php
include('../../connect.php');
$conn = connect_database();
// Create connection
if (isset($_GET['user_id'])) {
    $sql_root = "SELECT * FROM hl_bet WHERE status = 1 or status = 2 and user_id=" . $_GET['user_id'];
    $sql = $sql_root . " ORDER BY created_at,status  DESC";
    $result = $conn->query($sql);

    //Pagination For Bet History
    $row_per_page = 3;
    $number_record = $result->num_rows;
    $all_page = ceil($number_record / $row_per_page);
    if (!isset($_GET["page"]))
        $page = 1;
    else
        $page = $_GET["page"];
    $offset = ($page - 1) * $row_per_page;
    $sql_page = $sql_root . " ORDER BY created_at  DESC" . " LIMIT $offset,$row_per_page";
    $result_page = $conn->query($sql_page);

}
?>
<body class="skin_blue" style="">
<div class="report">
    <div class="search">
    </div>
    <table class="list table" style="width: 95vw">
        <thead>
        <tr>
            <!--            <th>注单号</th>-->
            <th width="10%">Bet ID</th>
            <!--            <th>时间</th>-->
            <th width="10%">Bet time</th>
            <!--            <th>类型</th>-->
            <th width="17.5%">Type</th>
            <!--            <th>玩法</th>-->
            <th width="22.5%">Bet type</th>
            <!--            <th>盘口</th>-->
            <th width="10%">Bet Amount</th>
            <!--            <th>下注金额</th>-->
            <!--            <th>退水</th>-->
            <th width="10%">Win/Lose</th>
            <!--            <th>可赢金额</th>-->
            <th width="5%">Status</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $node = array();
        $all_total = array();
        if (isset($_GET['user_id'])) {
            while ($row2 = $result_page->fetch_assoc()) {
                $node[] = $row2;
            }

            if ($result_page->num_rows > 0) {
                // output data of each row
                $win_lose_profit = array();
                foreach ($node as $key => $row) {
                    $data_bet = json_decode($row['data_bet'], true);
                    $stt = 0;
                    $key_element = 0;
                    if (count($data_bet) > 0) {
                        foreach ($data_bet as $k => $value) {
                            if ($value) {
                                ?>
                                <tr class="">
                                    <td><?php echo date('y') . date('d') . sprintf("%04d", $row['id']) . "" . ++$stt; ?></td>
                                    <td><?php echo date('d-m-Y H:m', $row['created_at']); ?></td>
                                    <td><?php
                                        if($row['type'] ==1){
                                            $txt = "@ <b>Two side</b>";
                                        }else {
                                            $txt = "@ <b>Bet rank</b>";
                                        }
                                        echo "HL Racing ".$txt;
                                        echo "<br/>";
                                        echo "<span class='draw_number'>Round: " . $row['round_id'] . "</span>";
                                        ?></td>
                                    <td style="text-align: left; padding-left:15px;"><b>
                                            <?php
                                            if ($row['type'] == 1) {
                                                $rate = 1.9999;
                                                $type = substr($k, 0, 2);
                                                $place = substr($k, 2, 1);
                                                $place_text = '';
                                                if ($place > 3) {
                                                    $place_text = $place . "th";
                                                } elseif ($place == 2) {
                                                    $place_text = $place . "nd";
                                                } elseif ($place == 3) {
                                                    $place_text = $place . "rd";
                                                } elseif ($place == 1) {
                                                    $place_text = $place . "st";
                                                }
                                                $pick = substr($k, 4);
                                                $text = '';
                                                if ($type == "DS") {
                                                    if ($pick == "D") {
                                                        $text = "Odd";
                                                    } else {
                                                        $text = "Even";

                                                    }
                                                } elseif ($type == "DX") {
                                                    if ($pick == 'D') {
                                                        $text = "Big";
                                                    } else {
                                                        $text = "Small";
                                                    }

                                                } elseif ($type == "LH") {
                                                    if ($pick == 'L') {
                                                        $text = "Dragon";
                                                    } else {
                                                        $text = "Tiger";
                                                    }
                                                }elseif($type == 'GD'){
                                                    $text ='Champion Bet';
                                                    $type_champion = substr($k, 0, 3);
                                                    if($type_champion == 'GDX'){
                                                        if(substr($k, 4, 1) == 'D'){
                                                            $rate=2.2;
                                                            $place_text = 'Big';
                                                        }else{
                                                            $rate=1.79;
                                                            $place_text = 'Small';
                                                        }
                                                    }else {
                                                        if(substr($k, 4, 1) == 'D'){
                                                            $rate=1.79;
                                                            $place_text = 'Odds';
                                                        }else{
                                                            $rate=2.2;
                                                            $place_text = 'Even';
                                                        }
                                                    }
                                                }
                                                echo "<span class='text'>" .
                                                    $place_text . " Place" . "[ $text ]" .
                                                    "</span> @ <span class='odds'>".$rate."</span>";
                                            } else if ($row['type'] == 2) {
                                                $bet_place = substr($k,strpos($k,'B')+1,strpos($k,'_')-1);
                                                $bet_horse = substr($k,strpos($k,'_')+1);

                                                $place_text = '';
                                                if ($bet_place > 3) {
                                                    $place_text = $bet_place . "th";
                                                } elseif ($bet_place == 2) {
                                                    $place_text = $bet_place . "nd";
                                                } elseif ($bet_place == 3) {
                                                    $place_text = $bet_place . "rd";
                                                } elseif ($bet_place == 1) {
                                                    $place_text = $bet_place . "st";
                                                }
                                                echo "<span class='text'>" .
                                                    $place_text . " Place" . "[ Car: <b>$bet_horse</b> ]" .
                                                    "</span> @ <span class='odds'>9.93</span>";
                                            }
                                            ?></b>
                                    </td>
                                    <td style="text-align: left; padding-left:15px;">
                                        <?php echo $value . "<br/>"; ?>
                                    </td>
                                    <td>
                                        <?php
                                        $odd_even = explode(",", $row['result_o_v']);
                                        $big_small = explode(",", $row['result_b_s']);
                                        $tiger_dragon = explode(",", $row['result_t_d']);
                                        $rank_bet = explode(",", $row['result_rank_bet']);
                                        $champion_bet = explode(",", $row['result_champion_bet']);
                                        if (isset($odd_even[$key_element]) && isset($big_small[$key_element]) && isset($tiger_dragon[$key_element]) && isset($rank_bet[$key_element]) && isset($champion_bet[$key_element])) {
                                            $win_lose_profit[] = ($odd_even[$key_element] + $big_small[$key_element] + $tiger_dragon[$key_element]+$rank_bet[$key_element]+$champion_bet[$key_element]) - $value;
                                            echo ($odd_even[$key_element] + $big_small[$key_element] + $tiger_dragon[$key_element]+$rank_bet[$key_element]+$champion_bet[$key_element]) - $value;
                                        } else {
                                            $win_lose_profit[] = -$value;
                                            echo -$value;
                                        }
                                        $key_element++;
                                        ?>
                                    </td>
                                    <td><?php if ($row['status'] == 0) {
                                            ?>
                                            <span class="">Running</span>
                                            <?php
                                        } elseif ($row['status'] == 1) {
                                            ?>
                                            <span class="">Finished</span>
                                            <?php
                                        } elseif ($row['status'] == 2) {
                                            ?>
                                            <span class="">Finished</span>
                                            <?php
                                        } ?></td>
                                </tr>
                            <?php }
                        }
                    }
                }
            } else {
                echo "<td colspan='8'><b>No Data<b></td>";
            }
        } else {
            echo "<td colspan='8'><b>You must login first!<b></td>";
        } ?>
        </tbody>
        <tfoot>
        <tr>
            <td></td>

            <th>Total</th>
            <td></td>
            <td style="text-align: left; padding-left:15px;"><?php
                $sum_total = 0;
                foreach ($all_total as $t) {
                    $sum_total += $t;
                }
                echo $sum_total;
                ?></td>
            <td></td>
            <td class="result color">
                <?php
                $total_win_lose = 0;
                foreach($win_lose_profit as $value){
                    $total_win_lose +=$value;
                }
                echo $total_win_lose;
                ?></td>
            <td></td>
        </tr>
        </tfoot>
    </table>
    <div class="page_info">
        <p align="center" style="clear:both">
            <?php
            for ($i = 1; $i <= $all_page; $i++) {
                if ($i == $page) {
                    echo "&nbsp;<b>" . $i . "</b>&nbsp;";
                } else {
                    ?>
                    <a href="bet-history.php?page=<?php echo $i; ?>&user_id=<?php echo $_GET['user_id'] ?>"><?php echo $i; ?></a>
                    <?php
                }
            }

            ?>
        </p>
    </div>
</div>

</body>
</html>