<?php
include('../../connect.php');
$conn = connect_database();
if (isset($_POST['round-name'])) {
    $created_at = strtotime("+7 hours");
    $data = $_POST;

    unset($data['round-name']);
    unset($data['user_id']);
    $json_data = json_encode($data);
    $user_id = isset($_GET['user_id']) ? base64_decode($_GET['user_id']) : $_POST['user_id'];
    $round_id = substr($_POST['round-name'], 6);


    $total_bet = 0;

    $data_bet = array();
    foreach ($data as $key => $value) {
        if ($value > 0) {
            $total_bet += $value;
            $data_bet[$key] = $value;
        } elseif ($value <= 0) {
            unset($data[$key]);
        }
    }
    $json_data = json_encode($data);
    $select_query = "SELECT * FROM hl_users WHERE id = $user_id";
    $rec = $conn->query($select_query);
    $d = $rec->fetch_assoc();
    if ($d['coin'] > $total_bet && count($data) > 0) {
        $json_data_bet = json_encode($data_bet);
        echo json_encode(array('errors' => 0,'msg'=>'Bet successful!,Good luck!'));
        $sql = "INSERT INTO hl_bet (type, data, round_id,created_at,status,user_id,data_bet)
            VALUES ('2', '$json_data',$round_id, $created_at,0,$user_id,'$json_data_bet')";
        $conn->query($sql);
        $return_coin = $d['coin'] - $total_bet;
        $update_query = "UPDATE hl_users SET coin = '$return_coin' WHERE id = $user_id";
        $conn->query($update_query);
    } else {
        if (count($data) <= 0) {
            echo json_encode(array('errors' => 1,'msg'=>'No data bet'));
        }else {
            echo json_encode(array('errors' => 1,'msg'=>'Not enough balance to process the bet, Please Try Again'));
        }
    }
}

