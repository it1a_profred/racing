<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <!-- <meta http-equiv="X-Frame-Options" content="allow"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
    <!--    <link rel="stylesheet" type="text/css" href="../../newdsn/css/main.css?v=0106" />-->
    <link rel="stylesheet" type="text/css" href="../../newdsn/css/bet.css"/>
    <link rel="stylesheet" type="text/css" href="../../newdsn/css/modal.css"/>
    <link rel="stylesheet" type="text/css" href="../../newdsn/css/custom.css"/>
    <link rel="stylesheet" type="text/css" href="../../newdsn/css/balls.css"/>
    <link rel="stylesheet" type="text/css" href="../../newdsn/css/g_PK10.css?v=0106"/>
    <script type="text/javascript" src="../../js/jquery.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui.js"></script>
    <!--    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
    <!--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

    <script type="text/javascript" src="../../js/libs.js"></script>
    <script type="text/javascript" src="../../js/json2.js"></script>
    <script type="text/javascript" src="../../default/js/skin.js?v=1219"></script>
    <script type="text/javascript" src="../../default/js/period.js"></script>
    <!--<script type="text/javascript" src="../../default/js/bet_backup.js"></script>-->
    <script type="text/javascript" src="../../default/js/bet_backup.js"></script>
    <!-- <script type="text/javascript" src="../../default/js/results.js"></script> -->
    <!-- <script type="text/javascript" src="../../default/js/resultPanel.js"></script> -->
    <!--    <script type="text/javascript" src="../../default/js/pagemap.js"></script>-->
    <script type="text/javascript" src="../../newdsn/js/drawurls.js"></script>
    <script type="text/javascript" src="../../newdsn/js/defaultbet.js"></script>
    <!--    <script type="text/javascript" src="../../newdsn/js/rightpanel.js"></script>-->
    <script type="text/javascript" src="../../newdsn/js/modal.js?v=1219"></script>
    <script type="text/javascript" src="../../js/dialog.js"></script>

    <script type="text/javascript">var lottery = 'BJPK10';
        var lotteryName = '北京赛车(PK10)';
        var lotteryType = '0';
        var template = 'PK10'</script>
    <script>
        $(document).ready(function () {
            $("#tbl-bet input.ba").prop("type", "number");
            $("#tbl-bet input.ba").prop("min", "1");
            $("#champion_bet input.ba").prop("type", "number");
            $("#champion_bet input.ba").prop("min", "1");
        });
    </script>
</head>
<body class="L_BJPK10 P_lm skin_blue" style="">
<!-- Start Popup -->
<script>
    function show_dialog() {
        var all_data = $("#bet-type-1").serialize();
        var data = $("#bet-type-1").serializeArray();
        var user_id = $("#user_id").val();
        var record = '';
        var test = $.each(data, function (i, field) {
            if (field.value && field.name != 'round-name') {
                if (isNaN(field.value) == false) {
                    record = '<tr>' +
                        '<td>' + field.name + '</td>' +
                        '<td> <b style="color:red;">1.9999</b></td>' +
                        '<td>' + field.value + '</td>' +
                        '<td><input type="checkbox" checked value="1"></td>' +
                        '</tr>';
                }
            }
        });
        var betList_begin = '<div class="betList" style="height:300px;">' +
            '<table class="table">' +
            '<thead>' +
            '<th>Number</th>' +
            '<th>Rate</th>' +
            '<th>Amount</th>' +
            '<th>Confirm</th>' +
            '</thead>' +
            '<tbody id="betList">';

        var betList_end = '</tbody></table>' +
            '</div>' +
            '<div class="bottom">' +
            '<span id="bcount"></span>' +
            '<span id="btotal"></span>' +
            '</div>' +
            '<div>' +
            '<label style="display:none">' +
            '<input type="checkbox" id="ignoreOdds" />如赔率变化，按最新赔率投注，不提示赔率变化' +
            '</label>' +
            '</div>';

        $('#betsBox').html(betList_begin);
        $.each(data, function (i, field) {
            if (field.value && field.name != 'round-name' && field.name != 'user_id') {
                var rate = 1.9999;
                if (field.name == 'GDS_D' || field.name == 'GDX_X') {
                    rate = 1.79;
                } else if (field.name == 'GDX_D' || field.name == 'GDS_S') {
                    rate = 2.22;
                } else {
                    rate = 1.9999;
                }
                if (Number(field.value) > 0) {
                    record = '<tr>' +
                        '<td>' + field.name + '</td>' +
                        '<td> <b style="color:red;">' + rate + '</b></td>' +
                        '<td>' + field.value + '</td>' +
                        '<td><input type="checkbox" checked value="1"></td>' +
                        '</tr>';
                    $('#betList').append(record);
                }
            }
        });
        $('#betsBox').append(betList_end);
        $('#betsBox').dialog({
            dialogClass: 'confirm-box',
            title: 'Note below (please confirm the note)',
            closeButton: false,
            autoOpen: false,
            resizable: false,
            icon: true,
            position: ['left', 300],
            modal: true,
            minHeight: 0,
            width: 400,
            buttons: {
                "Confirm": function () {
                    $.ajax({
                        type: "POST",
                        url: "data-bet-game1.php",
                        data: all_data,
                        success: function (response) {
                            var obj = JSON.parse(response);
                            console.log(obj.errors);
                            if (obj.errors == 0) {
                                alert(obj.msg);
                                window.top.location.reload();
                            } else {
                                alert(obj.msg);
                            }

                        }
                    });
                },
                "Cancel": function () {
                    $(this).dialog("close")
                }
            }
        }).dialog('open');
        $("#betsBox").position({
            my: 'center',
            at: 'center',
            collision: 'fit'
        });
    }
</script>
<!-- End popup -->
<div id="main">
    <div>
        <?php
        include('../../connect.php');
        $conn = connect_database();
        $select_query = "Select * FROM hl_rounds ORDER BY timestart desc LIMIT 1";
        $result = $conn->query($select_query);
        $data_round = $result->fetch_assoc();
        $time_bet = $data_round['timestart'];
        $ticket_id = $data_round['id'];
        ?>
    </div>
    <div id="header">
        <div class="lottery_info">
            <div class="lottery_info_left floatleft"><span class="name" id="lotteryName">HL Racing(HLR10)</span> —
                <span class="gameName" id="gameName"></span>
                <span class="result">&nbsp;Total win today：
                    <span id="bresult">
                        <?php
                        $start_today = strtotime(date('d-m-Y 00:00'));
                        $end_today = strtotime(date('d-m-Y 23:59'));
                        $query_today = "SELECT  * FROM hl_bet WHERE (created_at BETWEEN $start_today AND $end_today )AND user_id =" . base64_decode($_GET['user_id']) . " AND status = 1";
                        $result = $conn->query($query_today);
                        $node = array();
                        while ($row = $result->fetch_assoc()) {
                            $node[] = $row;
                        }
                        $total_win_today = 0;
                        foreach ($node as $record) {
                            $result_o_v = explode(",", $record['result_o_v']);
                            $result_b_s = explode(",", $record['result_b_s']);
                            $result_t_d = explode(",", $record['result_t_d']);
                            $result_rank_bet = explode(",", $record['result_rank_bet']);
                            $result_champion_bet = explode(",", $record['result_champion_bet']);
                            $total_record = count(json_decode($record['data'], true));
                            $total=0;
                            for ($i = 0; $i < $total_record; $i++) {
                                $total += $result_o_v[$i]+$result_b_s[$i]+$result_t_d[$i]+$result_rank_bet[$i]+$result_champion_bet[$i];
                            }
                            $total_win_today+=$total;
                        }
                        echo number_format($total_win_today,2);
                        ?>
                    </span>
                </span>
            </div>
            <div class="lottery_info_right floatright">Round ID:
                <span id="">
                    <?php echo $ticket_id; ?>
                </span>
                &nbsp;&nbsp;Time Closed：
                <span class="color_lv bold"><span id="cdClose">0:0</span></span>&nbsp;&nbsp;Time Remain：
                <span class="color_lv bold"><span id="cdDraw">0:0</span></span>
                <span id="cdRefresh" style="float:right;width: 50px;">30s</span>
            </div>
            <div class="clearfloat"></div>
        </div>
        <div class="control n_anniu">
        </div>
    </div>

    <div id="bet_panel" class="bet_panel input_panel">


        <script type="text/javascript">
            var games = 'DX1,DX2,DX3,DX4,DX5,DX6,DX7,DX8,DX9,DX10,DS1,DS2,DS3,DS4,DS5,DS6,DS7,DS8,DS9,DS10,GDX,GDS,LH1,LH2,LH3,LH4,LH5';
            $(function () {
                // Results.init({
                //     '': ['B{0}'],
                //     '大小': ['DX{0}', 'dx'],
                //     '单双': ['DS{0}', 'ds'],
                //     'Champion Bets': ['GYH'],
                //     'Champion Bets 大小': ['GDX', 'dx'],
                //     'Champion Bets 单双': ['GDS', 'ds']
                // }, 1, 10, ['1st Place', '2nd Place', '3rd Place', '4th Place', '5th Place', '6th Place', '7th Place', '8th Place', '9th Place', '10th Place'], 1);
            });
        </script>
        <input type="hidden" value="<?php echo $time_bet; ?>" id="time-bet"/>

        <div id="betsBox"></div>
        <form method="post" action="" id="bet-type-1">
            <input type="hidden" value="round_<?php echo $data_round['id']; ?>" name="round-name"/>
            <input type="hidden" value="<?php echo base64_decode($_GET['user_id']); ?>" name="user_id"/>
            <table class="table_lm" id="champion_bet">
                <tbody>
                <tr class="head">
                    <th colspan="12">Champions Bets</th>
                </tr>
                <tr>
                    <th class="GGDX_D name" id="t_GDX_D" title=" Big"><input type="hidden" id="k_GDX_D" value="GDX">Big
                        [12-19]
                    </th>

                    <td class="GGDX_D odds" id="o_GDX_D">2.2</td>
                    <td class="GGDX_D amount ha"><input name="GDX_D" class="ba"></td>

                    <th class="GGDX_X name" id="t_GDX_X" title=" Small"><input type="hidden" id="k_GDX_X" value="GDX">Small
                        [3-11]
                    </th>

                    <td class="GGDX_X odds" id="o_GDX_X">1.79</td>
                    <td class="GGDX_X amount ha"><input name="GDX_X" class="ba"></td>
                    <th class="GGDS_D name" id="t_GDS_D" title=" Odds"><input type="hidden" id="k_GDS_D" value="GDS">Odds
                    </th>

                    <td class="GGDS_D odds" id="o_GDS_D">1.79</td>
                    <td class="GGDS_D amount ha"><input name="GDS_D" class="ba"></td>
                    <th class="GGDS_S name" id="t_GDS_S" title=" Even"><input type="hidden" id="k_GDS_S" value="GDS">Even
                    </th>

                    <td class="GGDS_S odds" id="o_GDS_S">2.2</td>
                    <td class="GGDS_S amount ha"><input name="GDS_S" class="ba"></td>
                </tr>
                </tbody>
            </table>
            <div class="split_panel" id="tbl-bet">
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">1st Place</th>
                    </tr>
                    <tr>
                        <th class="GDX1_D name" id="t_DX1_D" title="1st Place Big"><input type="hidden" id="k_DX1_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX1_D odds" id="o_DX1_D">1.9999</td>
                        <td class="GDX1_D amount ha"><input name="DX1_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX1_X name" id="t_DX1_X" title="1st Place Small"><input type="hidden" id="k_DX1_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX1_X odds" id="o_DX1_X">1.9999</td>
                        <td class="GDX1_X amount ha"><input name="DX1_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS1_D name" id="t_DS1_D" title="1st Place Odd"><input type="hidden" id="k_DS1_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS1_D odds" id="o_DS1_D">1.9999</td>
                        <td class="GDS1_D amount ha"><input name="DS1_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS1_S name" id="t_DS1_S" title="1st Place Even"><input type="hidden" id="k_DS1_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS1_S odds" id="o_DS1_S">1.9999</td>
                        <td class="GDS1_S amount ha"><input name="DS1_S" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH1_L name" id="t_LH1_L" title="1st Place Dragon"><input type="hidden" id="k_LH1_L"
                                                                                             value="LH">Dragon
                        </th>

                        <td class="GLH1_L odds" id="o_LH1_L">1.9999</td>
                        <td class="GLH1_L amount ha"><input name="LH1_L" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH1_H name" id="t_LH1_H" title="1st Place Tiger"><input type="hidden" id="k_LH1_H"
                                                                                            value="LH">Tiger
                        </th>

                        <td class="GLH1_H odds" id="o_LH1_H">1.9999</td>
                        <td class="GLH1_H amount ha"><input name="LH1_H" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">2nd Place</th>
                    </tr>
                    <tr>
                        <th class="GDX2_D name" id="t_DX2_D" title="2nd Place Big"><input type="hidden" id="k_DX2_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX2_D odds" id="o_DX2_D">1.9999</td>
                        <td class="GDX2_D amount ha"><input name="DX2_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX2_X name" id="t_DX2_X" title="2nd Place Small"><input type="hidden" id="k_DX2_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX2_X odds" id="o_DX2_X">1.9999</td>
                        <td class="GDX2_X amount ha"><input name="DX2_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS2_D name" id="t_DS2_D" title="2nd Place Odd"><input type="hidden" id="k_DS2_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS2_D odds" id="o_DS2_D">1.9999</td>
                        <td class="GDS2_D amount ha"><input name="DS2_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS2_S name" id="t_DS2_S" title="2nd Place Even"><input type="hidden" id="k_DS2_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS2_S odds" id="o_DS2_S">1.9999</td>
                        <td class="GDS2_S amount ha"><input name="DS2_S" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH2_L name" id="t_LH2_L" title="2nd Place Dragon"><input type="hidden" id="k_LH2_L"
                                                                                             value="LH">Dragon
                        </th>

                        <td class="GLH2_L odds" id="o_LH2_L">1.9999</td>
                        <td class="GLH2_L amount ha"><input name="LH2_L" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH2_H name" id="t_LH2_H" title="2nd Place Tiger"><input type="hidden" id="k_LH2_H"
                                                                                            value="LH">Tiger
                        </th>

                        <td class="GLH2_H odds" id="o_LH2_H">1.9999</td>
                        <td class="GLH2_H amount ha"><input name="LH2_H" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">3rd Place</th>
                    </tr>
                    <tr>
                        <th class="GDX3_D name" id="t_DX3_D" title="3rd Place Big"><input type="hidden" id="k_DX3_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX3_D odds" id="o_DX3_D">1.9999</td>
                        <td class="GDX3_D amount ha"><input name="DX3_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX3_X name" id="t_DX3_X" title="3rd Place Small"><input type="hidden" id="k_DX3_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX3_X odds" id="o_DX3_X">1.9999</td>
                        <td class="GDX3_X amount ha"><input name="DX3_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS3_D name" id="t_DS3_D" title="3rd Place Odd"><input type="hidden" id="k_DS3_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS3_D odds" id="o_DS3_D">1.9999</td>
                        <td class="GDS3_D amount ha"><input name="DS3_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS3_S name" id="t_DS3_S" title="3rd Place Even"><input type="hidden" id="k_DS3_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS3_S odds" id="o_DS3_S">1.9999</td>
                        <td class="GDS3_S amount ha"><input name="DS3_S" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH3_L name" id="t_LH3_L" title="3rd Place Dragon"><input type="hidden" id="k_LH3_L"
                                                                                             value="LH">Dragon
                        </th>

                        <td class="GLH3_L odds" id="o_LH3_L">1.9999</td>
                        <td class="GLH3_L amount ha"><input name="LH3_L" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH3_H name" id="t_LH3_H" title="3rd Place Tiger"><input type="hidden" id="k_LH3_H"
                                                                                            value="LH">Tiger
                        </th>

                        <td class="GLH3_H odds" id="o_LH3_H">1.9999</td>
                        <td class="GLH3_H amount ha"><input name="LH3_H" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">4th Place</th>
                    </tr>
                    <tr>
                        <th class="GDX4_D name" id="t_DX4_D" title="4th Place Big"><input type="hidden" id="k_DX4_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX4_D odds" id="o_DX4_D">1.9999</td>
                        <td class="GDX4_D amount ha"><input name="DX4_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX4_X name" id="t_DX4_X" title="4th Place Small"><input type="hidden" id="k_DX4_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX4_X odds" id="o_DX4_X">1.9999</td>
                        <td class="GDX4_X amount ha"><input name="DX4_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS4_D name" id="t_DS4_D" title="4th Place Odd"><input type="hidden" id="k_DS4_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS4_D odds" id="o_DS4_D">1.9999</td>
                        <td class="GDS4_D amount ha"><input name="DS4_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS4_S name" id="t_DS4_S" title="4th Place Even"><input type="hidden" id="k_DS4_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS4_S odds" id="o_DS4_S">1.9999</td>
                        <td class="GDS4_S amount ha"><input name="DS4_S" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH4_L name" id="t_LH4_L" title="4th Place Dragon"><input type="hidden" id="k_LH4_L"
                                                                                             value="LH">Dragon
                        </th>

                        <td class="GLH4_L odds" id="o_LH4_L">1.9999</td>
                        <td class="GLH4_L amount ha"><input name="LH4_L" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH4_H name" id="t_LH4_H" title="4th Place Tiger"><input type="hidden" id="k_LH4_H"
                                                                                            value="LH">Tiger
                        </th>

                        <td class="GLH4_H odds" id="o_LH4_H">1.9999</td>
                        <td class="GLH4_H amount ha"><input name="LH4_H" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">5th Place</th>
                    </tr>
                    <tr>
                        <th class="GDX5_D name" id="t_DX5_D" title="5th Place Big"><input type="hidden" id="k_DX5_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX5_D odds" id="o_DX5_D">1.9999</td>
                        <td class="GDX5_D amount ha"><input name="DX5_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX5_X name" id="t_DX5_X" title="5th Place Small"><input type="hidden" id="k_DX5_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX5_X odds" id="o_DX5_X">1.9999</td>
                        <td class="GDX5_X amount ha"><input name="DX5_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS5_D name" id="t_DS5_D" title="5th Place Odd"><input type="hidden" id="k_DS5_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS5_D odds" id="o_DS5_D">1.9999</td>
                        <td class="GDS5_D amount ha"><input name="DS5_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS5_S name" id="t_DS5_S" title="5th Place Even"><input type="hidden" id="k_DS5_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS5_S odds" id="o_DS5_S">1.9999</td>
                        <td class="GDS5_S amount ha"><input name="DS5_S" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH5_L name" id="t_LH5_L" title="5th Place Dragon"><input type="hidden" id="k_LH5_L"
                                                                                             value="LH">Dragon
                        </th>

                        <td class="GLH5_L odds" id="o_LH5_L">1.9999</td>
                        <td class="GLH5_L amount ha"><input name="LH5_L" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GLH5_H name" id="t_LH5_H" title="5th Place Tiger"><input type="hidden" id="k_LH5_H"
                                                                                            value="LH">Tiger
                        </th>

                        <td class="GLH5_H odds" id="o_LH5_H">1.9999</td>
                        <td class="GLH5_H amount ha"><input name="LH5_H" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">6th Place</th>
                    </tr>
                    <tr>
                        <th class="GDX6_D name" id="t_DX6_D" title="6th Place Big"><input type="hidden" id="k_DX6_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX6_D odds" id="o_DX6_D">1.9999</td>
                        <td class="GDX6_D amount ha"><input name="DX6_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX6_X name" id="t_DX6_X" title="6th Place Small"><input type="hidden" id="k_DX6_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX6_X odds" id="o_DX6_X">1.9999</td>
                        <td class="GDX6_X amount ha"><input name="DX6_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS6_D name" id="t_DS6_D" title="6th Place Odd"><input type="hidden" id="k_DS6_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS6_D odds" id="o_DS6_D">1.9999</td>
                        <td class="GDS6_D amount ha"><input name="DS6_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS6_S name" id="t_DS6_S" title="6th Place Even"><input type="hidden" id="k_DS6_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS6_S odds" id="o_DS6_S">1.9999</td>
                        <td class="GDS6_S amount ha"><input name="DS6_S" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">7th Place</th>
                    </tr>
                    <tr>
                        <th class="GDX7_D name" id="t_DX7_D" title="7th Place Big"><input type="hidden" id="k_DX7_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX7_D odds" id="o_DX7_D">1.9999</td>
                        <td class="GDX7_D amount ha"><input name="DX7_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX7_X name" id="t_DX7_X" title="7th Place Small"><input type="hidden" id="k_DX7_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX7_X odds" id="o_DX7_X">1.9999</td>
                        <td class="GDX7_X amount ha"><input name="DX7_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS7_D name" id="t_DS7_D" title="7th Place Odd"><input type="hidden" id="k_DS7_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS7_D odds" id="o_DS7_D">1.9999</td>
                        <td class="GDS7_D amount ha"><input name="DS7_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS7_S name" id="t_DS7_S" title="7th Place Even"><input type="hidden" id="k_DS7_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS7_S odds" id="o_DS7_S">1.9999</td>
                        <td class="GDS7_S amount ha"><input name="DS7_S" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">8th Place</th>
                    </tr>
                    <tr>
                        <th class="GDX8_D name" id="t_DX8_D" title="8th Place Big"><input type="hidden" id="k_DX8_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX8_D odds" id="o_DX8_D">1.9999</td>
                        <td class="GDX8_D amount ha"><input name="DX8_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX8_X name" id="t_DX8_X" title="8th Place Small"><input type="hidden" id="k_DX8_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX8_X odds" id="o_DX8_X">1.9999</td>
                        <td class="GDX8_X amount ha"><input name="DX8_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS8_D name" id="t_DS8_D" title="8th Place Odd"><input type="hidden" id="k_DS8_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS8_D odds" id="o_DS8_D">1.9999</td>
                        <td class="GDS8_D amount ha"><input name="DS8_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS8_S name" id="t_DS8_S" title="8th Place Even"><input type="hidden" id="k_DS8_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS8_S odds" id="o_DS8_S">1.9999</td>
                        <td class="GDS8_S amount ha"><input name="DS8_S" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">9th Place</th>
                    </tr>
                    <tr>
                        <th class="GDX9_D name" id="t_DX9_D" title="9th Place Big"><input type="hidden" id="k_DX9_D"
                                                                                          value="DX">Big
                        </th>

                        <td class="GDX9_D odds" id="o_DX9_D">1.9999</td>
                        <td class="GDX9_D amount ha"><input name="DX9_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX9_X name" id="t_DX9_X" title="9th Place Small"><input type="hidden" id="k_DX9_X"
                                                                                            value="DX">Small
                        </th>

                        <td class="GDX9_X odds" id="o_DX9_X">1.9999</td>
                        <td class="GDX9_X amount ha"><input name="DX9_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS9_D name" id="t_DS9_D" title="9th Place Odd"><input type="hidden" id="k_DS9_D"
                                                                                          value="DS">Odd
                        </th>

                        <td class="GDS9_D odds" id="o_DS9_D">1.9999</td>
                        <td class="GDS9_D amount ha"><input name="DS9_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS9_S name" id="t_DS9_S" title="9th Place Even"><input type="hidden" id="k_DS9_S"
                                                                                           value="DS">Even
                        </th>

                        <td class="GDS9_S odds" id="o_DS9_S">1.9999</td>
                        <td class="GDS9_S amount ha"><input name="DS9_S" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                    <tr class="head">
                        <th colspan="3">10th Place</th>
                    </tr>
                    <tr>
                        <th class="GDX10_D name" id="t_DX10_D" title="10th Place Big"><input type="hidden" id="k_DX10_D"
                                                                                             value="DX">Big
                        </th>

                        <td class="GDX10_D odds" id="o_DX10_D">1.9999</td>
                        <td class="GDX10_D amount ha"><input name="DX10_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDX10_X name" id="t_DX10_X" title="10th Place Small"><input type="hidden"
                                                                                               id="k_DX10_X"
                                                                                               value="DX">Small
                        </th>

                        <td class="GDX10_X odds" id="o_DX10_X">1.9999</td>
                        <td class="GDX10_X amount ha"><input name="DX10_X" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS10_D name" id="t_DS10_D" title="10th Place Odd"><input type="hidden" id="k_DS10_D"
                                                                                             value="DS">Odd
                        </th>

                        <td class="GDS10_D odds" id="o_DS10_D">1.9999</td>
                        <td class="GDS10_D amount ha"><input name="DS10_D" class="ba"></td>
                    </tr>
                    <tr>
                        <th class="GDS10_S name" id="t_DS10_S" title="10th Place Even"><input type="hidden"
                                                                                              id="k_DS10_S"
                                                                                              value="DS">Even
                        </th>

                        <td class="GDS10_S odds" id="o_DS10_S">1.9999</td>
                        <td class="GDS10_S amount ha"><input name="DS10_S" class="ba"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="control bcontrol">
                <div class="lefts" style="display:none">已经选中 <span id="betcount"></span> 注</div>
                <div class="buttons" style="margin-left: 10%;">
                    <!--<input type="button" class="view_lottery" onclick="showResult()" id="add-key4" data-title="&nbsp;" value="查看开奖">-->
                    <!--                    <input type="button" class="button2" value="快选金额" onclick="parent.showsetting()">-->
                    <!--                    <label class="checkdefault">-->
                    <!--                        <input type="checkbox" class="checkbox">-->
                    <!--                        <span class="color_lv bold">预设</span>-->
                    <!--                    </label>&nbsp;&nbsp;-->
                    <!--                    <label class="quickAmount">-->
                    <!--                        <span class="color_lv bold">金额</span>-->
                    <!--                        <input>-->
                    <!--                    </label>-->
                    <input type="button" value="Bet" class="button" onclick="show_dialog()"/>
                    <!--                    <button type="submit" class="button" id="btn-bet-type-1" name="btn-bet">Bet</button>-->
                    <input type="button" class="button" value="Reset" onclick="resetBets()">
                </div>
            </div>
        </form>
    </div>
    <!-- <div id="resultPanel">
        <table class="tabTitle">
            <tbody>
            <tr>
                <th class="selected"><a href="javascript:void(0)">1st Place</a></th>
                <th><a href="javascript:void(0)">2nd Place</a></th>
                <th><a href="javascript:void(0)">3rd Place</a></th>
                <th><a href="javascript:void(0)">4th Place</a></th>
                <th><a href="javascript:void(0)">5th Place</a></th>
                <th><a href="javascript:void(0)">6th Place</a></th>
                <th><a href="javascript:void(0)">7th Place</a></th>
                <th><a href="javascript:void(0)">8th Place</a></th>
                <th><a href="javascript:void(0)">9th Place</a></th>
                <th><a href="javascript:void(0)">10th Place</a></th>
            </tr>
            </tbody>
        </table>
        <table class="ballTable">
            <tbody>
            <tr class="head">
                <th class="b1">1</th>
                <th class="b2">2</th>
                <th class="b3">3</th>
                <th class="b4">4</th>
                <th class="b5">5</th>
                <th class="b6">6</th>
                <th class="b7">7</th>
                <th class="b8">8</th>
                <th class="b9">9</th>
                <th class="b10">10</th>
            </tr>
            <tr>
                <td class="b1">15</td>
                <td class="b2">8</td>
                <td class="b3">10</td>
                <td class="b4">8</td>
                <td class="b5">4</td>
                <td class="b6">5</td>
                <td class="b7">1</td>
                <td class="b8">10</td>
                <td class="b9">6</td>
                <td class="b10">10</td>
            </tr>
            </tbody>
        </table>
        <table class="tabTitle">
            <tbody>
            <tr>
                <th class="selected"><a href="javascript:void(0)" class="ball">1st Place</a></th>
                <th><a href="javascript:void(0)">大小</a></th>
                <th><a href="javascript:void(0)">单双</a></th>
                <th><a href="javascript:void(0)">Champion Bets</a></th>
                <th><a href="javascript:void(0)">Champion Bets 大小</a></th>
                <th><a href="javascript:void(0)">Champion Bets 单双</a></th>
            </tr>
            </tbody>
        </table>
        <table class="tabContents">
            <tbody>
            <tr>
                <td class="even">8</td>
                <td>7</td>
                <td class="even">8</td>
                <td>1</td>
                <td class="even">9<br>9</td>
                <td>1</td>
                <td class="even">3</td>
                <td>2</td>
                <td class="even">4<br>4</td>
                <td>9</td>
                <td class="even">1</td>
                <td>8</td>
                <td class="even">3</td>
                <td>10</td>
                <td class="even">2</td>
                <td>10</td>
                <td class="even">9</td>
                <td>3</td>
                <td class="even">9</td>
                <td>1<br>1</td>
                <td class="even">4<br>4</td>
                <td>1</td>
                <td class="even">8</td>
                <td>10<br>10</td>
                <td class="even">6</td>
            </tr>
            </tbody>
        </table>
    </div> -->
</div>
<table id="changlong">
    <thead>
    <tr>
        <th colspan="2" class="table_side">Bet Suggestions</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#LH2_H">2nd Place - Tiger</a></th>
        <td>5 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DX7_X">7th Place - Small</a></th>
        <td>5 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DX2_X">2nd Place - Small</a></th>
        <td>4 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DS1_S">1st Place - Even</a></th>
        <td>4 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DX1_D">1st Place - Big</a></th>
        <td>4 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#LH1_L">1st Place - Dragon</a></th>
        <td>4 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DS2_D">2nd Place - Odd</a></th>
        <td>3 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=gy#GDS_D">Champion bet - Odd</a></th>
        <td>3 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DX4_D">4th Place - Big</a></th>
        <td>3 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#LH4_L">4th Place - Dragon</a></th>
        <td>3 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=gy#GDX_X">Champion bet - Small</a></th>
        <td>2 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DS7_S">7th Place - Even</a></th>
        <td>2 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DS5_D">5th Place - Odd</a></th>
        <td>2 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DS10_S">10th Place - Even</a></th>
        <td>2 期</td>
    </tr>
    <tr>
        <th><a href="load?lottery=BJPK10&amp;page=lm#DS4_D">4th Place - Odd</a></th>
        <td>2 期</td>
    </tr>
    </tbody>
</table>
</body>
<!-- Custom JS -->
<script type="text/javascript" src="../../js/dialog.js"></script>
<script type="text/javascript" src="../../js/custom.js"></script>
</html>

