function openIframe(b) {
    var a = '<iframe src="' + b + '"></iframe>';
    $(".open-iframe").hide();
    $(".close-iframe").show();
    $(".new-tab-iframe").show();
    $(".iframe-wrapper").append(a)
}
function closeIframe() {
    $(".close-iframe").hide();
    $(".new-tab-iframe").hide();
    $(".iframe-wrapper iframe").addClass("close", removeIframe);
    $(".open-iframe").show()
}
function removeIframe() {
    $(".iframe-wrapper iframe").remove()
}
function openNewTab() {
    window.open($(".iframe-wrapper iframe").attr("src"), null, "height=700,width=350,status=yes,toolbar=no,menubar=no,location=no");
    closeIframe()
}
;