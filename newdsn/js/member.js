var MAX_DIVIDEND;
var SOUND_URL;
var DEFAULT;
var _LS;
var _LIC = 8;
var _lastResult;
var sideUserTitle;
var resetTimer;
var currentLottery;
var accountTimer;
var userparams;
var betting = false;
var _lastBetsTimer;
var _resultTimer;
var currentResult;
$.ajaxSetup({
    cache: false
});
$(function() {
    if (!$(".vip_static").length) {
        $(".notification").css("right", "10px")
    }
    var d = function() {
        if ($('.Notice > div[style="display: block;"]').length === 0 && $("#msg1").length == 0) {
            $.ajax({
                url: "member/getUnreadMessageCount?filterSysMsgFlag=true",
                cache: false,
                success: function(h) {
                    if (h && h.success) {
                        $("#hiddenCount").val(h.data);
                        if ($("#hiddenCount").val() >= 1 && $("#hiddenCount").val() <= 9) {
                            $(".notification .count").text($("#hiddenCount").val());
                            $(".notification").css("display", "block")
                        } else {
                            if ($("#hiddenCount").val() > 9) {
                                $(".notification .count").text("9+")
                            } else {
                                $(".notification").css("display", "none")
                            }
                        }
                    }
                }
            })
        }
    };
    // d();
    // setInterval(d, 10000);
    if (/MSIE (6|7)/.test(navigator.userAgent)) {
        var a = function() {
            var h = $("#footer").position().top - $("#main").position().top;
            $("#main").height(h);
            $("#frame").height($("#main").height());
            $("#main .frame").width($("#main").width() - 232)
        };
        a();
        $(window).resize(a)
    }
    $("#header .menu2 a").click(function() {
        $("#header .menu2 a.selected,#header .sub a.selected").removeClass("selected");
        $(this).addClass("selected");
        var h = $(this).attr("href");
        if (h != null && h.indexOf("{lottery}") != -1) {
            h = h.replace("{lottery}", currentLottery.id);
            $("#" + $(this).attr("target")).attr("src", h);
            return false
        }
    });
    var c = $("#lotterys a").click(function() {
        changePage($(this));
        refreshMenu()
    }).not("[target]").eq(0);
    var g = LIBS.cookie("defaultLT");
    if (g) {
        var e = $("#l_" + g);
        if (e.length > 0) {
            c = e
        }
    }
    initMenu();
    $(".header .sub a").click(function() {
        var i = $(this);
        $("#header .menu2 a.selected,#header .sub a.selected").removeClass("selected");
        $(this).addClass("selected");
        var h = i.attr("href");
        $("#frame").attr("src", h).data("a", i);
        return false
    });
    $(".sidenavibtn").click(function() {
        var i = $(this);
        clearSideBtn();
        $(i).addClass("sidenaviactive");
        var h = i.attr("rel");
        $("#frame").attr("src", h).data("a", i);
        return false
    });
    $(".sidenavibtn2").click(function() {
        clearSideBtn();
        return true
    });
    $("#frame").on("load", function() {
        resetPanel(true);
        var j = LIBS.cookie("_skin_");
        if (!j) {
            j = getDefaultSkin();
            if (!j) {
                j = skins[1][0]
            }
        }
        var i = ["blue", "gold", "red"];
        var h = i.indexOf(j);
        $(".themeicon").eq(h).click()
    });
    sideUserTitle = $("#side .user_info .title");
    sideUserTitle.data("text", sideUserTitle.text());
    LIBS.get("params", function(j) {
        if (!j) {
            return
        }
        var l = {};
        for (var h = 0; h < j.length; h++) {
            var k = j[h];
            l[k.l + "-" + k.g] = k
        }
        userparams = l
    });
    $("#betAmount").keypress(function(h) {
        if (h.keyCode == 13) {
            $("#btnBet").click()
        }
    });
    c.click();
    var f = LIBS.getUrlParam("page");
    var b = $("#userStatus").val();
    if (f == "payment/deposit" && b == 3) {
        f = "center/transfer"
    }
    if (f) {
        $("#frame").attr("src", f)
    }
    showAccount()
});
function goToTransfer(b) {
    clearSideBtn();
    var a = "member/center/transfer";
    $("#frame").attr("src", a).data("a", b);
    return false
}
function initMenu() {
    var e = $(".header .lotterys .show");
    var d = [];
    var f = $("#lotterys");
    f.find("a").each(function() {
        var i = $(this);
        var q = {
            id: i.attr("id").substr(2),
            info: i.attr("lang").split("_", 2),
            name: i.text()
        };
        i.data("info", q);
        d.push(q)
    });
    var c = LIBS.cookie("_menu_");
    var j = LIBS.cookie("_menumore_");
    if (!c) {
        var o = Math.min(_LIC, d.length);
        for (var m = 0; m < o; m++) {
            $(".items").append("<li><div class='item'></div></li>");
            var b = $(".item").eq(m);
            $("#l_" + d[m].id).appendTo(b);
            $(b).append("<div class='removebtn'></div>")
        }
        var n = 0;
        for (var m = _LIC; m < d.length; m++) {
            $(".gamebox").append("<div class='itemmg'></div>");
            var b = $(".itemmg").eq(n);
            $("#l_" + d[m].id).appendTo(b);
            $(b).append("<div class='addbtn'></div>");
            n++
        }
    } else {
        var a = c.split(",");
        var l = j.split(",");
        var p = a.length;
        var h = l.length;
        var k = $(".lotterys a").length;
        for (var m = 0; m < p; m++) {
            var g = a[m];
            $(".items").append("<li><div class='item'></div></li>");
            var b = $(".item").eq(m);
            $("#l_" + g).appendTo(b);
            $(b).append("<div class='removebtn'></div>")
        }
        var n = 0;
        for (var m = p; m < k; m++) {
            var g = l[n];
            $(".gamebox").append("<div class='itemmg'></div>");
            var b = $(".itemmg").eq(n);
            $("#l_" + g).appendTo(b);
            $(b).append("<div class='addbtn'></div>");
            n++
        }
    }
}
function changePage(d, c) {
    if (d.attr("target")) {
        return
    }
    var f = d.data("info");
    LIBS.cookie("defaultLT", f.id);
    if (!currentLottery || f.id != currentLottery.id) {
        currentLottery = f;
        refreshBets()
    }
    var e = $("#sub_" + f.id);
    $(".header .sub div:visible").hide();
    e.show();
    var b;
    if (c !== undefined) {
        b = e.find("a:eq(" + c + ")")
    } else {
        b = e.find("a.default");
        if (b.length == 0) {
            b = e.find("a:eq(0)")
        }
    }
    b.click()
}
function refreshBets() {
    clearTimeout(_lastBetsTimer);
    if (!currentLottery) {
        return
    }
    LIBS.get("lasts?lottery=" + currentLottery.id, function(f) {
        if (f) {
            var e = $("#lastBets ul").empty();
            for (var d = 0; d < f.length; d++) {
                var c = f[d];
                var a = $("<li>").appendTo(e);
                a.append($("<p>").html('注单号：<br/><span class="bid">' + c.id + "#</span>"));
                a.append($("<p>").addClass("contents").html('<span class="text">' + c.t + '</span>@<span class="odds">' + c.o + "</span>"));
                a.append($("<p>").text("下注额：￥" + c.a))
            }
        }
    });
    _lastBetsTimer = setTimeout(refreshBets, 60000)
}
function playSound() {
    if (!SOUND_URL) {
        return
    }
    var a = $("#SOUND");
    if (a.length == 0) {
        a = $('<audio id="SOUND"><source src="' + SOUND_URL + '" type="audio/mpeg"/></audio>').appendTo($("body"))[0];
        if (a.load) {
            a.load()
        }
    } else {
        a = a[0]
    }
    if (a.play) {
        a.play()
    }
}
function bet(b, d, a, c) {
    postBet({
        lottery: b,
        drawNumber: d,
        bets: a
    }, c)
}
function postBet(a, b) {
    a.bets.toJSON = function() {
        var c = [];
        for (var d = 0; d < this.length; d++) {
            c[d] = LIBS.clone({}, this[d], ["game", "contents", "amount", "odds", "multiple", "mcount", "state"])
        }
        return c
    }
    ;
    return $.ajax({
        url: "bet",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(a),
        success: function(c) {
            try {
                if ($.isFunction(b)) {
                    if (b(c) === false) {
                        return
                    }
                }
            } catch (d) {}
            showBetResult(a, c)
        },
        error: function(c) {
            alert("投注失败：" + c.code + ",请检查下注状况後重试。")
        },
        complate: function() {
            toggleBetButton(true)
        }
    })
}
function showAccount(b) {
    clearTimeout(accountTimer);
    accountTimer = setTimeout(function() {
        LIBS.get("account", function(d) {
            var c = $("#frame")[0].contentWindow.PeriodPanel;
            if (c) {
                c.showAccount(d)
            } else {
                showAccount(d)
            }
        })
    }, 15000);
    if (!b) {
        return
    }
    $("#account .balance").text(LIBS.round(b.balance, 1));
    $("#account .betting").text(b.betting || 0);
    if (b.bonus > 0) {
        $("#redPackIcon").show();
        loadBonus()
    } else {
        $("#redPackIcon").hide()
    }
    var a = $("#account .currentUsername").text();
    if (a != "" && a != b.username) {
        alert("您已使用帐号" + b.username + " 登陆。系统自动切换至帐号" + b.username + " 的权限，如需登录其他帐号，请登出後重试。");
        location.href = "member/index"
    }
}
function showResult(a) {
    var g = currentLottery;
    var m = g.info[0];
    clearTimeout(_resultTimer);
    _resultTimer = setTimeout(function() {
        LIBS.get("lastResult?lottery=" + g.id, showResult)
    }, 30000);
    if (!a || a.lottery != g.id) {
        return
    }
    if (_lastResult && _lastResult.lottery == a.lottery && (_lastResult.result != a.result || _lastResult.drawNumber != a.drawNumber)) {
        playSound();
        if ($("#resultList").is(":visible")) {
            $("#resultFrame")[0].contentWindow.location.reload(true)
        }
    }
    _lastResult = a;
    var l = _DRAW_URLS[g.id];
    if (typeof window.IS_MOBILE !== "undefined") {
        $("#result_balls").attr("href", l);
        $("#result_info").html(("<strong>" + a.drawNumber + "</strong>期"))
    } else {
        $("#result_balls").attr("href", l);
        $("#result_info").html(("<div>" + g.name + "</div><div>" + a.drawNumber + "期开奖</div>"))
    }
    if (!l) {
        $("#result_balls").removeAttr("href")
    }
    var c = $("#result_balls").attr("class", "T_" + m + " L_" + g.id);
    if (m == "HK6" || m == "3D" || m == "KL8") {
        $("#result_balls").css("margin-top", "5px")
    } else {
        $("#result_balls").css("margin-top", "-5px")
    }
    c.empty();
    var h = a.result.split(",");
    var f;
    if (m == "HK6") {
        f = get_animal_by_ball
    } else {
        if (m == "3D") {
            f = function(p, r) {
                return ["佰", "拾", "个"][r]
            }
        }
    }
    for (var j = 0; j < h.length; j++) {
        var e = h[j];
        var d = $("<span>").appendTo(c);
        d.append($("<b>").addClass("b" + e).text(e));
        if (f) {
            d.append($("<i>").text(f(e, j)))
        }
        if (m == "PCEGG") {
            if (j < h.length - 1) {
                d.append($('<i style="margin:-25px 0px 0px 40px">').text("+"))
            } else {
                d.append($('<i style="margin:-25px 0px 0px 40px">').text("="));
                var k = Number(h[0]) + Number(h[1]) + Number(h[2]);
                var q = $("<span>").appendTo(c);
                q.append($("<b>").addClass("bg b_" + k).text(k))
            }
        }
        if (m == "HK6") {
            if (j == 5) {
                $("<span>").addClass("plus").text("+").appendTo(c)
            }
        }
    }
    if (g.id == "PK10JSCNN") {
        generateNNResult(a)
    } else {
        var o = a.resultOther.split(",");
        if (o.length > 0) {
            var b = $("<div>").addClass("result_stat clearfix").appendTo(c);
            if (m == "GXKLSF") {
                b = $("<div>").addClass("result_stat clearfix ml8").appendTo(c)
            } else {
                if (m != "PK10") {
                    b = $("<div>").addClass("result_stat clearfix ml4").appendTo(c)
                }
            }
            for (var j = 0; j < o.length; j++) {
                var n = o[j];
                if (n) {
                    b.append($("<div>").addClass("statitm").text(n))
                }
            }
        }
    }
}
function generateNNResult(a) {
    var h = a.detail;
    var m = a.result;
    if (!h || h.length != 6) {
        return
    }
    var e = $("#frame").contents();
    var l = e.find("#gameName");
    if (l.length == 1) {
        var k = e.find(".poker-wrapper.show");
        if (m != currentResult || k.length == 0) {
            e.find(".poker").removeAttr("class");
            e.find(".game-info").removeAttr("class");
            e.find(".poker-wrapper").removeClass("win").removeClass("show");
            for (var g = 0; g < h.length; g++) {
                var c = h[g];
                var b = c.cards;
                for (var f = 0; f < b.length; f++) {
                    var d = b[f];
                    e.find("#card_" + g + "_" + (f + 1)).addClass("poker type-" + d.cardType + " card-" + d.cardNum)
                }
                e.find("#g_" + g).addClass("game-info niu-" + c.resultNum);
                if (c.betResult == "1") {
                    e.find("#p_" + g).addClass("win")
                }
            }
            if (k.length == 0) {
                e.find(".poker-wrapper").addClass("show")
            } else {
                setTimeout(function() {
                    e.find(".poker-wrapper").addClass("show")
                }, 700)
            }
            currentResult = m
        }
    }
}
function getUserParam(b, a) {
    if (!userparams) {
        return
    }
    return userparams[b + "-" + a]
}
function toggleBetButton(a) {
    if (a) {
        $(".control input").hide();
        $(".control span").show()
    } else {
        $(".control span").hide();
        $(".control input").show()
    }
}
function getBetTextHtml(b) {
    function c(h, g) {
        return '<span class="text">' + h + '</span>@<span class="odds">' + g + "</span>"
    }
    if (b.multiple) {
        return c(b.title, b.odds)
    }
    var f = b.text;
    var a = f.split("@");
    if (b.title) {
        f = b.title + " " + a[0]
    }
    var e = c(f, b.odds);
    if (a.length > 1) {
        for (var d = 1; d < a.length; d++) {
            e += "<br />" + c(a[d], b.oddsDetail[d - 1])
        }
    }
    return e
}
function showBetResult(c, m) {
    if (m.status == 3) {
        d = m.message
    } else {
        if (m.status == 1) {
            showBets(c, m.odds);
            return
        } else {
            if (m.status == 2) {
                d = "后台已封盘，请等待开盘再试。"
            }
        }
    }
    if (d) {
        showMsg(d);
        return
    }
    showAccount(m.account);
    $("#betResultDrawNumber").text(c.drawNumber + "期");
    var l = $("#betReulstList");
    l.empty();
    var h = c.bets;
    var v = 0;
    var e = 0;
    var d = m.message;
    for (var q = 0; q < h.length; q++) {
        var b = h[q];
        var f = m.odds[q].split(",");
        var n = f.shift();
        b.odds = n;
        b.oddsDetail = f;
        var k = $("<li>").appendTo(l);
        if (m.status == 0) {
            k.append($("<p>").html('注单号：<span class="bid">' + m.ids[q] + "</span>"))
        }
        k.append($("<p>").addClass("contents").html(getBetTextHtml(b)));
        if (b.multiple > 1) {
            e += b.multiple;
            v += b.amount * b.multiple;
            k.append($("<p>").addClass("contents").text("复式『 " + b.multiple + " 组 』"));
            k.append($("<p>").addClass("contents").text(b.text.join("、")));
            var u = b.amount;
            k.append($("<p>").text("分　组：" + u + " × " + b.multiple + "组"));
            k.append($("<p>").text("合计额：" + (b.amount * b.multiple)));
            k.append($("<p>").text("可赢额：" + LIBS.round(u * b.odds - u, 1)));
            var s = $("<table>").appendTo(k);
            var a = $("<tr>").addClass("head").appendTo(s);
            a.append($("<th>").text("ID").addClass("id"));
            a.append($("<th>").text("号码组合").addClass("nums"));
            a.append($("<th>").text("下注额"));
            var t = LIBS.comboArray(b.text, b.mcount);
            for (var p = 0; p < t.length; p++) {
                $("<tr>").appendTo(s).append($("<td>").text(p + 1)).append($("<td>").text(t[p].join(","))).append($("<td>").text("￥ " + u).addClass("amount"))
            }
        } else {
            e += 1;
            v += b.amount;
            if (b.multiple) {
                k.append($("<p>").addClass("contents").text(b.text.join("、")))
            }
            k.append($("<p>").text("下注金额： " + b.amount));
            k.append($("<p>").text("可赢金额： " + LIBS.round(b.amount * b.odds - b.amount, 1, true)))
        }
        if (m.status > 1 || (m.status == 1 && n != b.odds)) {
            k.append($("<span>").addClass("errmsg").text(d))
        }
    }
    var g = $("#betResultPanel");
    if (m.status == 0) {
        $("#betResultCount").text(e + "笔");
        $("#betResultTotal").text(v);
        g.children(".s1").hide();
        g.children(".s0").show()
    } else {
        g.children(".s0").hide();
        g.children(".s1").show()
    }
    showPanel(g, "下注结果反馈")
}
function reBet(a) {
    var b = $(a).data("last");
    if (b == null) {
        return
    }
    b.ignore = true;
    postBet(b)
}
function showPanel(a, b) {
    resetPanel();
    sideUserTitle.text(b);
    $(".betdone").hide();
    a.show()
}
function printBet(b) {
    var a = document.getElementById(b).innerHTML;
    w = window.open();
    w.document.write(a);
    w.print();
    w.close()
}
function resetPanel(a) {
    clearTimeout(resetTimer);
    resetTimer = null;
    sideUserTitle.text(sideUserTitle.data("text"));
    $(".betdone").show();
    $("#betResultPanel").hide();
    if (!a) {
        refreshBets()
    }
}
function showMsg(b, c) {
    var a = $("#messageBox");
    if (a.length == 0) {
        a = $('<div id="messageBox">').appendTo("body").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            icon: true,
            minHeight: 0,
            width: 400,
            title: "用户提示",
            buttons: {
                "确定": function() {
                    $(this).data("ok", true).dialog("close")
                },
                "取消": function() {
                    $(this).dialog("close")
                }
            }
        }).on("dialogclose", function(f) {
            var d = $(this).data("cb");
            if ($.isFunction(d)) {
                d($(this).data("ok"))
            }
        })
    }
    a.text(b).dialog("open").data({
        ok: false,
        cb: c
    });
    if (c) {
        a.dialog("widget").find(".ui-dialog-buttonset button:eq(1)").show()
    } else {
        a.dialog("widget").find(".ui-dialog-buttonset button:eq(1)").hide()
    }
}
function getBetText(a) {
    var c = "";
    if (a.title) {
        c += a.title + " "
    }
    c += a.text;
    if (a.multiple && a.multiple > 1) {
        c += '<div class="multiple">复式『 <span>' + a.multiple + ' 组</span> 』&nbsp;<a>查看明细</a><ol style="display:none">';
        var d = LIBS.comboArray(a.text, a.mcount);
        for (var b = 0; b < d.length; b++) {
            c += "<li><span>" + d[b].join("、") + "<span></li>"
        }
        c += "</ol>"
    }
    return c
}
function showBets(e, n) {
    var h = $("#betsBox");
    if (h.length == 0) {
        h = $('<div id="betsBox"></div>').appendTo("body").dialog({
            closeButton: false,
            autoOpen: false,
            resizable: false,
            icon: true,
            modal: true,
            minHeight: 0,
            width: 400,
            buttons: {
                "确定": function() {
                    var b = [];
                    $("#betList tr").each(function() {
                        var q = $(this);
                        if (q.find("input:checked").length != 0) {
                            var p = Number(q.find(".amount input").val());
                            if (p <= 0 || isNaN(p)) {
                                return
                            }
                            var o = q.data("b");
                            o.amount = p;
                            b.push(o)
                        }
                    });
                    if (b.length > 0) {
                        var i = $(this).data("req");
                        i.bets = b;
                        i.ignore = $("#ignoreOdds").prop("checked");
                        postBet(i)
                    }
                    $(this).data("sc", true).dialog("close")
                },
                "取消": function() {
                    $(this).dialog("close")
                }
            }
        }).on("dialogclose", function() {
            betting = false
        }).on("dialogbeforeclose", function() {
            if (!$(this).data("sc")) {
                var b = $(this);
                showMsg("你确定取消下注吗？", function(i) {
                    if (i) {
                        b.data("sc", true).dialog("close")
                    }
                });
                return false
            }
        });
        var j = '<div class="betList"><table class="table"><thead><th>号码</th><th>赔率</th><th>金额</th><th>确认</th></thead><tbody id="betList"></tbody></table></div><div class="bottom"><span id="bcount"></span><span id="btotal"></span></div><div><label style="display:none"><input type="checkbox" id="ignoreOdds" />如赔率变化，按最新赔率投注，不提示赔率变化</label></div>';
        h.html(j);
        h.keypress(function(b) {
            if (b.keyCode == 13) {
                $(this).parent().find(".ui-dialog-buttonset button:eq(0)").click()
            }
        })
    }
    betting = true;
    if (n) {
        h.dialog("option", "title", "赔率变化，请重新确认投注赔率")
    } else {
        h.dialog("option", "title", "下注明细（请确认注单）")
    }
    var g = $("#betList");
    g.empty();
    var a = e.bets;
    for (var f = 0, c = a.length; f < c; f++) {
        var m = a[f];
        var k = $("<tr>").appendTo(g).data("b", m).append($("<td>").addClass("contents").html(getBetText(m)));
        var d = m.odds;
        if (n) {
            m.odds = n[f];
            d += " -> " + m.odds
        }
        k.append($("<td>").addClass("odds").text(d));
        k.append($("<td>").addClass("amount").append($("<input>").val(m.amount))).append($("<td>").addClass("check").append($('<input type="checkbox">').prop("checked", true)))
    }
    function l() {
        var i = 0;
        var b = 0;
        g.find("tr").each(function() {
            var q = $(this);
            if (q.find("input:checked").length != 0) {
                var p = Number(q.find(".amount input").val());
                if (p <= 0 || isNaN(p)) {
                    return
                }
                var o = q.data("b");
                var r = o.multiple ? o.multiple : 1;
                i += p * r;
                b += r
            }
        });
        $("#bcount").text("组数：" + b);
        $("#btotal").text("总金额：" + i)
    }
    g.find("input").change(l);
    g.find(".multiple a").hover(function() {
        $(this).parent().find("ol").show()
    }, function() {
        $(this).parent().find("ol").hide()
    });
    l();
    h.dialog("open").data({
        req: e,
        sc: false
    })
}
function refreshMenu() {
    $(".gamecontainer").hide();
    var b = $("#l_" + currentLottery.id);
    $("#items a.selected").removeClass("selected");
    if (b.parent().hasClass("itemmg")) {
        $("#moregames").text(b.data("info").name);
        $(".lotterys .menumoregame").addClass("selected2")
    } else {
        $("#moregames").text("更多游戏");
        $(".lotterys .menumoregame").removeClass("selected2");
        b.addClass("selected")
    }
}
$(document).on("click", ".removebtn", function() {
    var b = $(".item").length;
    if (b == 1) {
        showMsg("最少要保留一个游戏！")
    } else {
        var a = $(this).parent().find("a");
        var b = $(".itemmg").length;
        $(".gamebox").append("<div class='itemmg'></div>");
        $(a).appendTo(".itemmg:eq(" + b + ")");
        $(".itemmg:eq(" + b + ")").append("<div class='addbtn'></div>");
        $(this).parent().parent().remove();
        $(".addbtn").css("display", "block");
        $(".removebtn").css("display", "block")
    }
});
$(document).on("click", ".addbtn", function() {
    var b = $(".item").length;
    if (b == _LIC) {
        showMsg("最多可以添加" + _LIC + "个游戏！")
    } else {
        var a = $(this).parent().find("a");
        $(".items").append("<li class='ui-sortable-handle'><div class='item'></div></div>");
        $(a).appendTo(".item:eq(" + b + ")");
        $(".item:eq(" + b + ")").append("<div class='removebtn'></div>");
        $(this).parent().remove()
    }
    $(".addbtn").css("display", "block");
    $(".removebtn").css("display", "block")
});
function loadAccount() {
    var a = $("#frame")[0].contentWindow.PeriodPanel;
    if (a) {
        a.loadAccounts()
    }
}
function loadBonus() {
    $.getJSON("payment/getbonusinfo", function(a) {
        if (a.data) {
            if (a.data.length > 1) {
                if (a.data.length > 9) {
                    $("#redPackIcon .redpack_no span").text("9+")
                } else {
                    $("#redPackIcon .redpack_no span").text(a.data.length)
                }
                $("#redPackIcon .redpack_no").show()
            } else {
                if (a.data.length == 1) {
                    $("#redPackIcon .redpack_no").hide()
                }
            }
        }
    })
}
function doLogout() {
    $.get("logout", function(a) {})
}
function clearSideBtn() {
    $(".sidenavibtn").removeClass("sidenaviactive");
    $(".sidenavibtn2").removeClass("sidenaviactive")
}
$(function() {
    $("#resultList").draggable();
    $("a").click(function() {
        $("#resultList").hide();
        clearSideBtn()
    })
});
function showOddsDetail(b, d) {
    var c = $("#oddsBox");
    if (c.length == 0) {
        c = $('<div id="oddsBox">').appendTo("body").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            icon: true,
            minHeight: 0,
            width: 600,
            title: "牌型翻倍赔率介绍",
            buttons: {
                "确定": function() {
                    $(this).data("ok", true).dialog("close")
                },
                "取消": function() {
                    $(this).dialog("close")
                }
            }
        }).on("dialogclose", function(g) {
            var f = $(this).data("cb");
            if ($.isFunction(f)) {
                f($(this).data("ok"))
            }
        })
    }
    var a = getOddsDetailBody(b);
    c.html(a).dialog("open").data({
        ok: false,
        cb: d
    });
    if (d) {
        c.dialog("widget").find(".ui-dialog-buttonset button:eq(1)").show()
    } else {
        c.dialog("widget").find(".ui-dialog-buttonset button:eq(1)").hide()
    }
}
function getOddsDetailBody(n) {
    var a = $("<div>").addClass("nn-wrapper");
    var m = $("<table>").addClass("nn-table").appendTo(a);
    var k = $("<thead>").addClass("no-bg").appendTo(m);
    k.append($("<th>").text("牌型").css("width", "30%"));
    k.append($("<th>").text("赔率").css("width", "20%").css("text-align", "center"));
    k.append($("<th>").text("牌型介绍"));
    var h = $("<tbody>").appendTo(m);
    var i = $("<tr>").appendTo(h);
    i.append($("<td>").text("无牛"));
    i.append($("<td>").css("text-align", "center").text("1:2"));
    i.append($("<td>").text("五个球中的任意3个球不能成为10的倍数"));
    var j = "";
    var f = "";
    var c = "";
    var l = "";
    if (n != null) {
        j = n.FB_1_1;
        f = n.FB_1_2;
        c = n.FB_1_3;
        l = n.FB_1_4
    }
    var g = $("<tr>").appendTo(h);
    var d = $("<td>").appendTo(g);
    d.append($("<div>").text("牛一至牛六"));
    d.append($("<div>").text("牛七至牛八"));
    d.append($("<div>").text("牛九"));
    var b = $("<td>").css("text-align", "center").appendTo(g);
    b.append($("<div>").text("1:" + j));
    b.append($("<div>").text("1:" + f));
    b.append($("<div>").text("1:" + c));
    g.append($("<td>").text("五个球中的任意3个球相加能成10的倍数，另外两张相加的点数为牛几"));
    var e = $("<tr>").appendTo(h);
    e.append($("<td>").text("牛牛"));
    e.append($("<td>").css("text-align", "center").text("1:" + l));
    e.append($("<td>").text("五个球中的任意3个球相加能成10的倍数，另外两张相加的点数也是10点"));
    a.append($("<p>").addClass("nn-paragraph").text("下注金额会暂时冻结下注金额的4倍加本金，开奖后连本带利一并返还。"));
    a.append($("<p>").addClass("nn-paragraph").text("例如玩家下注1元，冻结4元，下注总金额为5元。如果庄家（牛六及以下）赢，返还4元，庄家（牛七、牛八）赢，返还3元；庄家（牛九）赢，返还2元；庄家（牛牛）赢，不返还。"));
    a.append($("<p>").addClass("nn-paragraph red").text("注：当庄家与闲家点数相等时，牛六以上的点数第一张牌比大小（例如：庄家：15462牛八，闲家：46297牛八，4比1大，闲家赢）。牛六以下（含牛六）庄家赢。"));
    return a
}
function showOddsDetailPB(c) {
    var b = $("#oddsBoxPB");
    if (b.length == 0) {
        b = $('<div id="oddsBoxPB">').appendTo("body").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            icon: true,
            minHeight: 0,
            width: 600,
            title: "牌型平倍赔率介绍",
            buttons: {
                "确定": function() {
                    $(this).data("ok", true).dialog("close")
                },
                "取消": function() {
                    $(this).dialog("close")
                }
            }
        }).on("dialogclose", function(f) {
            var d = $(this).data("cb");
            if ($.isFunction(d)) {
                d($(this).data("ok"))
            }
        })
    }
    var a = getOddsDetailBodyPB();
    b.html(a).dialog("open").data({
        ok: false,
        cb: c
    });
    if (c) {
        b.dialog("widget").find(".ui-dialog-buttonset button:eq(1)").show()
    } else {
        b.dialog("widget").find(".ui-dialog-buttonset button:eq(1)").hide()
    }
}
function getOddsDetailBodyPB() {
    var a = $("<div>").addClass("nn-wrapper");
    var i = $("<table>").addClass("nn-table").appendTo(a);
    var h = $("<thead>").addClass("no-bg").appendTo(i);
    h.append($("<th>").text("牌型").css("width", "30%"));
    h.append($("<th>").text("赔率").css("width", "20%"));
    h.append($("<th>").text("牌型介绍"));
    var f = $("<tbody>").appendTo(i);
    var g = $("<tr>").appendTo(f);
    g.append($("<td>").css("cssText", "border-bottom: 0px !important").text("无牛"));
    g.append($("<td>").css("text-align", "center").css("cssText", "border-bottom: 0px !important").text(""));
    g.append($("<td>").css("cssText", "border-bottom: 0px !important").text("五个球中的任意3个球不能成为10的倍数"));
    var e = $("<tr>").appendTo(f);
    var c = $("<td>").css("cssText", "border-bottom: 0px !important").appendTo(e);
    c.append($("<div>").text("牛一至牛六"));
    c.append($("<div>").text("牛七至牛八"));
    c.append($("<div>").text("牛九"));
    var b = $("<td>").css("text-align", "center").css("cssText", "border-bottom: 0px !important").appendTo(e);
    b.append($("<div>").text(""));
    b.append($("<div>").text("1:2"));
    b.append($("<div>").text(""));
    e.append($("<td>").css("cssText", "border-bottom: 0px !important").text("五个球中的任意3个球相加能成10的倍数，另外两张相加的点数为牛几"));
    var d = $("<tr>").appendTo(f);
    d.append($("<td>").text("牛牛"));
    d.append($("<td>").css("text-align", "center").text(""));
    d.append($("<td>").text("五个球中的任意3个球相加能成10的倍数，另外两张相加的点数也是10点"));
    a.append($("<p>").addClass("nn-paragraph red").text("注：当庄家与闲家点数相等时，牛六以上的点数第一张牌比大小（例如：庄家：15462牛八，闲家：46297牛八，4比1大，闲家赢）。牛六以下（含牛六）庄家赢。"));
    return a
}
;