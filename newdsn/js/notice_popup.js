function noticePopup(b, a) {
    getSkinColor();
    $(function () {
        var c = $("<div>").appendTo($(".Notice"));
        c.attr("id", "notice" + b);
        $("<div>").addClass("back_body").appendTo(c);
        if ((typeof(skinColor) == "undefined") || (skinColor == null)) {
            skinColor = "blue"
        }
        c.append('<div class="notice_div ' + skinColor + '_back"><a href="#"><div id="notClose' + b + '" class="close_icon"></div></a><div class="message-notice"><div class="notice_icon1"></div></div><div class="notice_font">' + a + '</div><div id="notice_button' + b + '" class="notice_button"><a href="#" class="notice_yellow animate">知道</a></div></div>');
        $("#notice_button" + b + " , #notClose" + b).click(function () {
            $("#notice" + b).hide();
            $(".details").hide()
        });
        $(".close_icon , .notice_button").click(function () {
            $(".noticeChild").hide();
            $(".details").hide()
        });
        $(".nicon_button").click(function () {
            $(".noticeChild").hide();
            $(".details").show()
        })
    })
}
$(document).ready(function () {
    $("#footer .more").click(function () {
        getSkinColor();
        $("#dtlFont").html("");
        $.ajax({
            url: "notice1", type: "GET", contentType: "application/json", data: "", success: function (c) {
                for (var b = 0; b < c.length; b++) {
                    if (b + 1 == c.length) {
                        dfClass = 3
                    } else {
                        if (1 == 0) {
                            dfClass = 1
                        } else {
                            dfClass = 2
                        }
                    }
                    var a = new Date(c[b].created).toLocaleString();
                    $("#dtlColor").attr("class", "details_div " + skinColor + "_back");
                    $("#dtlFont").append('<div class="df' + dfClass + '"><div class="df_data">' + a + "</div><div>" + c[b].text + "</div></div>")
                }
            }, error: function (a) {
                alert("失败：" + a.code + ",请检查状况後重试。")
            }, complete: function () {
                $(".details").show()
            }
        })
    });
    $(".close_icon , .notice_button").click(function () {
        $(".noticeChild").hide();
        $(".details").hide()
    })
});
function msgPopup(b, e, d, a, c) {
    $(function () {
        getSkinColor();
        if ((typeof(skinColor) == "undefined") || (skinColor == null)) {
            skinColor = "blue"
        }
        var f = $("<div>").appendTo($(".Notice"));
        var h = 1;
        f.attr("id", "msg" + e);
        $("<div>").addClass("back_body").appendTo(f);
        $("<div>").addClass("back_body").appendTo(f);
        var g = "";
        if (d.length >= 450) {
            g = " large "
        }
        f.append('<div class="notice_div message-notice ' + g + skinColor + '_back"><a href="#"><div id="notClose' + e + '" class="close_icon"></div></a><div class="notice_page"><a href="#" id="btnPrev' + e + '" class="notice_prev" ><<</a>' + e + "/" + b + '<a href="#" class="notice_next" id="btnNext' + e + '">>></a></div><div class="notice_icon1"><div class="nicon_icon1" style="background-image:none"></div><div class="nicon_button"><a href="#" class="notice_white animate">更多</a></div></div><div class="notice_font title">' + c + '</div><div class="notice_font">' + d + '</div><div id="notice_button' + e + '" class="notice_button"><a href="#" class="notice_yellow animate">知道</a></div></div>');
        if (e > 1) {
            $("#msg" + e).hide()
        }
        if (e == b) {
            h = 3
        } else {
            if (e == 1) {
                h = 1
            } else {
                h = 2
            }
        }
        $("#dtlColor").attr("class", "details_div " + skinColor + "_back");
        $("#dtlFont").append('<div class="df' + h + '"><div class="df_data">' + a + "</div><div>" + d + "</div></div>");
        $("#notice_button" + e + " , #notClose" + e + " , #btnNext" + e).click(function () {
            if (e < b) {
                var i = e + 1;
                $("#msg" + i).show()
            }
            $("#msg" + e).hide();
            $(".details").hide();
            if ($('.Notice > div[style="display: block;"]').length === 0) {
                $(".Notice").empty()
            }
        });
        $("#btnPrev" + e + " , #btnPrev" + e).click(function () {
            if (e > 1) {
                var i = e - 1;
                $("#msg" + i).show();
                $("#msg" + e).hide();
                $(".details").hide()
            }
        });
        $(".nicon_button").click(function () {
            $(".msg").hide();
            $(".details").show()
        })
    })
}
function noticePopupWithMore(b, d, c, a) {
    $(function () {
        getSkinColor();
        if ((typeof(skinColor) == "undefined") || (skinColor == null)) {
            skinColor = "blue"
        }
        var e = $("<div>").appendTo($(".Notice"));
        var g = 1;
        e.attr("id", "notice" + d);
        $("<div>").addClass("back_body").appendTo(e);
        $("<div>").addClass("back_body").appendTo(e);
        var f = "";
        if (c.length >= 450) {
            f = " large "
        }
        e.append('<div class="notice_div ' + f + skinColor + '_back"><a href="#"><div id="notClose' + d + '" class="close_icon"></div></a><div class="notice_page"><a href="#" id="btnPrev' + d + '" class="notice_prev" ><<</a>' + d + "/" + b + '<a href="#" class="notice_next" id="btnNext' + d + '">>></a></div><div class="notice_icon"><div class="nicon_icon1"></div><div class="nicon_button"><a href="#" class="notice_white animate">更多</a></div></div><div class="notice_font">' + c + '</div><div id="notice_button' + d + '" class="notice_button"><a href="#" class="notice_yellow animate">知道</a></div></div>');
        if (d > 1) {
            $("#notice" + d).hide()
        }
        if (d == b) {
            g = 3
        } else {
            if (d == 1) {
                g = 1
            } else {
                g = 2
            }
        }
        $("#dtlColor").attr("class", "details_div " + skinColor + "_back");
        $("#dtlFont").append('<div class="df' + g + '"><div class="df_data">' + a + "</div><div>" + c + "</div></div>");
        $("#notice_button" + d + " , #notClose" + d).click(function () {
            if (d < b) {
                var h = d + 1;
                $("#notice" + h).show()
            }
            $("#notice" + d).hide();
            $(".details").hide()
        });
        $("#btnNext" + d).click(function () {
            if (d < b) {
                var h = d + 1;
                $("#notice" + h).show()
            }
            $("#notice" + d).hide();
            $(".details").hide()
        });
        $("#btnPrev" + d).click(function () {
            if (d > 1) {
                var h = d - 1;
                $("#notice" + h).show();
                $("#notice" + d).hide();
                $(".details").hide()
            }
        });
        $(".nicon_button").click(function () {
            $(".Notice").hide();
            $(".details").show()
        })
    })
}
function redPackPopup() {
    $(function () {
        $.getJSON("payment/getbonusinfo", function (a) {
            if (a.data.length > 0) {
                $.ajax({
                    url: "payment/drawbonus",
                    type: "GET",
                    contentType: "application/json",
                    data: {transId: a.data[0].id},
                    success: function (b) {
                        if (a.success == true) {
                            $("#redPack .redpack_amount").text(a.data[0].amount);
                            $("#redPack .redpack_remark").text(a.data[0].remark);
                            if (!window.IS_MOBILE) {
                                loadBonus()
                            }
                            $("#redPack").show();
                            if (!window.IS_MOBILE) {
                                loadAccount()
                            }
                        } else {
                            alert(a.message)
                        }
                    },
                    error: function (b) {
                        alert("失败：" + b.code + ",请检查状况後重试。")
                    },
                    complete: function () {
                        $("#redPackIcon").hide()
                    }
                })
            }
        })
    })
};