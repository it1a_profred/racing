<!DOCTYPE html>
<!-- saved from url=(0042)https://1681380.com/view/PK10/pk10kai.html -->
<html style="overflow: visible;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11">
    <title>Last 50 Results</title>
    <link rel="stylesheet" href="recently-result/css/headorfood.css">
    <link rel="stylesheet" href="recently-result/css/pk10kai.css">
    <link rel="shortcut icon" href="https://1681380.com/img/icon/168favicon.ico?v=2018961524">
    <link rel="stylesheet" href="recently-result/css/user_adv.css">
    <link rel="stylesheet" href="recently-result/css/idangerous.swiper.css">
    <script async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script src="recently-result/js/jquery.min.js"></script>
    <script src="recently-result/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="recently-result/js/jscolor.js"></script>
</head>

<body style="overflow: visible;">
<?php
include('connect.php');
$conn = connect_database();
$sql = "SELECT * FROM hl_rounds WHERE status = 1 ORDER BY timestart  DESC LIMIT 50";

$result = $conn->query($sql);
while ($row2 = $result->fetch_assoc()) {
    $node[] = $row2;
}
?>
<div class="bodybox">
    <div class="kaijiangjl margt20">
        <div class="head">
            <ul class="zoushimap" id="kaijiangjl">
                <li class="kaijiltit kaijjlColor">Last 50 Results</li>
            </ul>
            <div id="kjls">
                <a href="index.php">Homepage</a>
            </div>
        </div>
        <div class="listcontent">
            <div class="jrsmhmtj jrsmhmtj_kai" id="jrsmhmtj">
                <table id="jrsmhmtjTab" cellpadding="1" cellspacing="1" border="0"
                       style="background: rgb(212, 212, 212);">
                    <tbody>
                    <tr>
                        <th>Round Time</th>
                        <th>Round ID</th>
                        <th id="numberbtn" class="numberbtn">Result</th>
                        <th colspan="3">Champion bet</th>
                        <th colspan="5">Tiger/Dragon Result</th>
                    </tr>
                    <?php
                    if ($result->num_rows > 0) {
                        foreach ($node as $key => $row) {
                            echo "<tr>";
                            ?>
                            <td><?php echo date('d-m-Y H:i:s', $row['timestart']) ?></td>
                            <td><?php echo sprintf("%06d", $row['id']); ?></td>
                            <td>
                                <?php
                                $data_array = explode(",", $row['result']);
                                if ($row['result'] != null) {
                                    echo "<ul class='imgnumber'>";
                                    foreach ($data_array as $value) {
                                        ?>
                                        <li class="numsm<?php echo sprintf("%02d", $value); ?>">
                                            <i><?php echo sprintf("%02d", $value); ?></i>
                                        </li>
                                        <?php
                                    }
                                    echo "</ul>";
                                } ?>
                            </td>
                            <td><?php echo $sum_data = $data_array[0]+$data_array[1];?></td>
                            <td style="color:<?php echo ($sum_data > 11) ? "#f12d35" : "#184dd5"; ?>"><?php echo ($sum_data > 11) ? "Big" : "Small"; ?></td>
                            <td style="color:<?php echo ($sum_data % 2 == 0) ? "#f12d35" : "#184dd5"; ?>"><?php echo ($sum_data % 2 == 0) ? "Even" : "Odd"; ?></td>
                            <td style="color:<?php echo ($data_array[0] > $data_array[9]) ? "#f12d35" : "#184dd5"; ?>"><?php echo ($data_array[0] > $data_array[9]) ? "Dragon" : "Tiger"; ?></td>
                            <td style="color:<?php echo ($data_array[1] > $data_array[8]) ? "#f12d35" : "#184dd5"; ?>"><?php echo ($data_array[1] > $data_array[8]) ? "Dragon" : "Tiger"; ?></td>
                            <td style="color:<?php echo ($data_array[2] > $data_array[7]) ? "#f12d35" : "#184dd5"; ?>"><?php echo ($data_array[2] > $data_array[7]) ? "Dragon" : "Tiger"; ?></td>
                            <td style="color:<?php echo ($data_array[3] > $data_array[6]) ? "#f12d35" : "#184dd5"; ?>"><?php echo ($data_array[3] > $data_array[6]) ? "Dragon" : "Tiger"; ?></td>
                            <td style="color:<?php echo ($data_array[4] > $data_array[5]) ? "#f12d35" : "#184dd5"; ?>"><?php echo ($data_array[4] > $data_array[5]) ? "Dragon" : "Tiger"; ?></td>
                            <?php
                            echo "</tr>";
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <div id="fooderbox">
        <div class="footer1">
            <ul>
                <li class="lileft">
                    <img src="logo/best-bet-logo.png" alt="HL-RACING" width="100%" height="auto">

                    <p style="text-align: center;font-size: 20px;">You only live once!!</p>

                </li>
                <li class="about_li">
                    <div><span class="about"></span><span class="">About Us</span></div>
                    <p class="p1">
                        <a href="#">About Us</a>
                    </p>

                    <p>
                        <a href="#">Customer Service</a>
                    </p>

                    <p>
                        <a href="#">Disclaimer</a>
                    </p>
                </li>
                <li class="about_li">
                    <div><span class="rewards"></span><span class="">Gameplay</span></div>
                    <br/>

                    <p>
                        <a href="#">Games rules</a>
                    </p>
                </li>
                <li>
                    <!--<div><img src="img/icon/gongzh.png?v=2018961524" alt="" style="width: 100%;"/></div>
                    <p>扫一扫，关注168公众号</p>-->
                </li>
            </ul>
        </div>
        <div class="footer3">
            <div class="footer3c">
                Copyright <span id="localyears"><?php echo date('Y'); ?></span> www.hl-bet.com All rights reserved

            </div>
        </div>
    </div>
    <!-- End Footer -->
</div>
</body>
<div style="position: absolute; top: 0px;"></div>
</html>